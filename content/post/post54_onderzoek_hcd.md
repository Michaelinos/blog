---
title: Onderzoek HCD
date: 2019-11-05
---

Vandaag ging ik beginnen aan mijn onderzoek naar human-centered design. Ten eerste heb ik een aantal onderzoeksvragen opgesteld:
-	Wat is HCD precies?
-	Hoe wordt HCD ingezet in het ontwerpproces?
-	Wat zijn praktijkvoorbeelden waarbij HCD van pas is gekomen?
-	Op wat voor manier wordt HCD toegepast bij RAAK?

Aan de hand van deze onderzoeken ben ik het internet afgestruind naar verschillende artikelen en websites die deze vragen voor mij kunnen beantwoorden. Aan het eind van de ochtend had ik al een aardig beeld wat human-centered design inhoudt. Daarom heb ik een korte beschrijving uitgeschreven en vervolgens een stuk dat verdiepend op de fases ingaat.

In de middag heb ik met Martin, onze projectmanager, afgesproken. Hij heeft in het verleden een cursus gevolgd over design thinking. Hij heeft mij in dit gesprek vertelt hoe hij HCD toepast en hoe dit wordt toegepast bij andere bedrijven.
Tevens heb ik ook een interview afgelegd met Rick over HCD. Hij heeft mij heel veel uitgelegd over hoe HCD wordt toegepast bij RAAK en op wat voor manier zij afwijken van de norm. Dit heb ik allemaal netjes gedocumenteerd om later verder uit te werken.

8 uur gewerkt vandaag.

---
title: Emailtemplate + stageplan
date: 2019-09-09
---

De week was begonnen met keek op de week. Een vergadering die elke maandag ochtend plaatsvindt waarbij wordt verteld wat er deze week te wachten staat en mededelingen worden gedeeld. Sinds vandaag hebben we dan ook een nieuwe officemanager.

Na deze vergadering ben ik aan de slag gegaan met de feedback op het emailtemplate dat Mohsen mij heeft gegeven. Zo heb ik de CTA meer uitnodigend gemaakt en een aanta visuele aanpassingen gemaakt. Fabian heeft er naar gekeken en nog een paar kleine aanpassingen gemaakt waardoor de template nu klaar is voor de developers.
Tevens heb ik nog wat feedback op de daily UI Challenge 1 verwerkt.

Om 13u had ik een vergadering over de huidige design challenge van de lander pagina. Hier is veel gediscussieerd over de presentatie van het filmpje en wat er gecommuniceerd moet worden met de lander zelf en SlideDeck presentatie.

De rest van de dag heb ik gewerkt aan de eerste versie van mijn stageplan.

8 uur gewerkt vandaag.

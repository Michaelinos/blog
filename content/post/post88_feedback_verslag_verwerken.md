---
title: Feedback op stageverslag + verwerken
date: 2020-01-08
---

In de ochtend heb ik mijn verslag aan Rick opgeleverd. Hier heb ik ontzettend veel goede feedback op ontvangen. Dit had voornamelijk betrekking op mijn leerdoelbeschrijving. De rest van de dag ben ik bezig geweest om deze feedback te verwerken in mijn verslag. Tevens mijn algemene reflectie op de stageperiode geschreven.

9 uur gewerkt vandaag.

---
title: DUI8 + Trello + wireframes BE + teamuitje
date: 2019-10-10
---

Vandaag ben ik in de ochtend bezig geweest met daily UI Challenges. Ik heb een scherm voor instellingen verder afgemaakt, een 404 pagina ontworpen en ik heb een begin gemaakt aan een desktop muziek speler. Deze vervolgens met Rick gedeeld voor feedback.

In de middag heb ik gewerkt aan het project voor Betlej Elektrotechniek. Ik heb ten eerste met Rick samengezeten voor een vraag van mij over Trello. Ik snapte nog niet zo goed hoe ik nou precies de taken kan formuleren en hier subtaken voor kan verzinnen. We hebben er samen eentje doorgenomen, helemaal met acceptance criteria en alles waardoor het gelijk een stuk duidelijker werd. Ook heb ik gevraagd of ik per se alles moet uitschetsen of ook kan beginnen met digitale schetsen. Rick zei dat het ook ok is om digitaal verder te gaan zolang ik het niet allemaal visueel uitwerk maar puur gebruik voor wireframes. De rest van de dag ben ik bezig geweest mijn schetsen te digitaliseren.

Vandaag ook eerder gestopt omdat we een gezamenlijk teamuitje hadden met een kookworkshop.

8 uur gewerkt vandaag.

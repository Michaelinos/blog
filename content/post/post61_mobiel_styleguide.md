---
title: Mobiele versie BE af + styleguide + stageoverleg
date: 2019-11-14
---

In de ochtend heb ik verder gewerkt aan de mobiele versie van de BE website. Hiervoor heb ik alle schermen (homepagina, projectenoverzicht, projectenpagina) uitgewerkt en deze geprototyped.

In de middag verder gewerkt aan de styleguide. Deze heb ik voor de desktop afgemaakt. In de middag had ik stageoverleg. Hier heb ik vragen over het stageverslag, design system en ethiek met Daily UI’s besproken.

8 uur gewerkt vandaag.

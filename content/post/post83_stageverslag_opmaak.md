---
title: Stageverslag opmaak
date: 2019-12-16
---

Vandaag voornamelijk bezig geweest met werkzaamheden voor het stageverslag:
-	Mijn blog volledig bijgewerkt
-	Opmaak stageerslag in InDesign gemaakt volgens de huisstijl van RAAK
-	Alle stage bestanden georganiseerd en gesorteerd
-	Steekwoordelijk 2 STARRTS uitgeschreven over brainstormen en high-fid prototype
-	Weekplanning gemaakt
-	WeConnekt schermen geprototyped

8 uur gewerkt vandaag.

---
title: Onderzoek HCD + DUI12
date: 2019-11-08
---

In de ochtend heb ik weer gewerkt aan mijn onderzoek naar human-centered design. Daarvoor heb ik de inleiding geschreven, inhoudsopgave gemaakt en alle bronvermeldingen netjes volgens APA-regels genoteerd.

In de middag heb ik gewerkt aan een nieuwe daily UI, het maken van een artikelpagina bij een e-commerce website. Voor deze opdracht heb ik gekozen om de website van Zara gebruiksvriendelijker te maken. Hierbij heb ik de navigatie van foto’s makkelijker gemaakt en grotere knoppen voor het kiezen van maat en kleur etc. Tevens ook extra informatie zoals gebruikt materiaal duidelijk in beeld gebracht. Vervolgens bij Mohsen om feedback gevraagd:
-	Onbelangrijke info kan kleiner
-	Carousel maken van de foto’s
-	Bezorgtijden aangeven
-	Knop maken om artikelen op te slaan

Met deze feedback wil ik volgende week aan de slag gaan.

8 uur gewerkt vandaag, 40 uur totaal deze week.

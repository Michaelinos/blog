---
title: User stories BE + schetsen
date: 2019-10-04
---

Vandaag ben ik officieel gestart met mijn eigen project voor Betlej Elektrotechniek. Onlangs heb ik akkoord gekregen op de debriefing, dus was het tijd om te beginnen met de user stories. Door eerdere uitleg wist ik wel ongeveer wat het zijn en hoe je ze moet formuleren maar voor de zekerheid heb ik toch wat extra onderzoek gedaan op het internet. Zo kwam ik bij https://agilescrumgroup.nl/wat-is-een-user-story/. Deze website heeft me goed op weg geholpen met het formuleren van de user stories.

Dus ben ik begonnen met het maken van user stories op basis van de ontwerpdoelen in de debrief. Toen ik er eenmaal vijf had, ben ik naar Mike (projectmanager) gegaan met de vraag of hij mij verder kon helpen. Hij zei dat de user stories zelf prima geformuleerd zijn, maar momenteel heb je er nog niet zo veel aan omdat deze pas later in het ontwerpproces aan bod komen. Mike heeft geadviseerd om user stories te maken die betrekking hebben op Sprint 0, het maken van schetsen. Tevens heeft hij aangeraden om op internet te zoeken naar de termen acceptance criteria, sprint 0, epic user story en story refinement.

Dus na verdere onderzoek heb ik extra user stories gemaakt, wat een stuk makkelijk afging. Deze heb ik gemaakt voor de eerste twee sprints, schetsen en concepting. Vervolgens heb ik mijn user stories in Trello gezet en heb ik weer feedback aan Mike gevraagd, die ze nu prima vindt. Vervolgens ben ik aan de slag gegaan met het schetsen. De rest van de dag ben ik bezig geweest met inspiratie opzoeken, de websitestructuur uittekenen, en meerdere versies maken van de websiteonderdelen.

8 uur gewerkt vandaag, 32 uur totaal (1 dag ziek).

---
title: Design Challenge IFFR + Lander RAAK
date: 2019-09-04
---

Dag begonnen met standup meeting. Hier kort vertelt over waar ik gister mee bezig ben geweest. Na deze standup had ik eigenlijk niet zo heel veel te doen. Ik had daarom aan Mohsen, een collega, gevraagd of hij nog wat weet. Hij heeft mij aangeraden om een daily UI challenge te doen via dailyui.co of sharpen.design. Hier ben ik dan ook direct mee aan de slag gegaan.

Mijn eerste opdracht via sharpen.design was: “Design a payment UI for a film festival”.
Ik heb gekozen om deze opdracht te maken voor het International Film Festival Rotterdam. Ik heb daarom voor de vormgeving gekozen om trouw te blijven aan hun huisstijl. Ik heb daarom dezelfde fonts gebruikt en zo veel mogelijk dezelfde kleuren toegepast in mijn ontwerpen. Ook heb ik gekozen om een mobiele versie te maken voor deze opdracht.
Vervolgens heb ik op Dribbble en Behance gekeken voor inspiratie op het gebied van betaalpagina’s. Toen ik eenmaal een idee had ben ik aan de slag gegaan in Adobe XD.

Na de lunch had ik een eerste versie af van het ontwerp. Ik heb in totaal vier schermen gemaakt: Film overzicht > Selectie soort ticket > Betaalmethode > Bevestingspagina.
Vervolgens heb ik aan Mohsen gevraagd om feedback te geven op wat ik heb gemaakt:

Ik heb aan Mohsen gevraagd om feedback te geven op wat ik heb gemaakt:

-	Titel van film in 1 kader verdelen. Ik dacht dat het aparte titels waren omdat ze gescheiden waren.
-	Voor betere zichtbaarheid titel kan je een gradient vlak erachter plaatsen.
-	Selectie van hoeveelheid tickets nog niet helemaal duidelijk, verzin hier een andere oplossing voor
-	Locatie evenement beter aangeven door er een icoontje naast te zetten, dan is het duidelijk dat het om de locatie gaat.
-	Nog wat kleine verfijningen over uitlijning en fontgrootte etc. met elkaar besproken

Ik heb naar zijn feedback geluisterd en ben hier mee aan de slag gegaan voor een tweede versie. Hij was er tevreden over en stelde nog een paar kleine visuele aanpassingen voor.
Toevallig kwam Rick, mijn stagebegeleider, net langs. Ik had hem gevraagd of hij ook nog feedback wil geven op wat ik had gemaakt. Hij heeft er naar gekeken en zei het volgende:

-	Op eerste scherm duidelijker aangegeven dat de onderste balk een button is om verder te gaan, voelt nu meer als een footer
-	Scherm met hoeveelheid tickets kan beter. Het is nog niet helemaal duidelijk dat je kan drukken om de hoeveelheid aan te passen. Stuur de gebruiker door te beginnen met 1 ticket, wat de gebruiker vervolgens kan aanpassen.
-	Op het derde scherm met de betaalmethode kan je de gebruiker ook beter sturen. iDeal wordt het meeste gebruikt dus maak die knop groter dan de overige betaalmethoden. Tevens ook leuk om het menu openklap baar te maken zodat je gelijk een bank kan selecteren.
-	Vanaf het tweede scherm wellicht beter om het IFFR-logo in de afbeelding te verwerken (bijv transparante banner). Op deze manier kan je de gebruiker ook beter sturen om het proces af te maken.
-	Op het laatste bevestigingsscherm moet je de gebruiker laten weten waar hij of zij de tickets kan terugvinden. Bijv. in de mail of een animatie dat de tickets nu in het menu staan.

Ik heb zijn feedback opgeschreven en wil dit dan ook verwerken in een volgende versie.
Rick en ik hadden nog niet samengezeten om te praten over leerdoelen dus daarom zijn we dit hierna gelijk gaan doen. We hebben gepraat over het stagebedrijvingsformulier dat nog ingevuld moet worden en hoe ik mijn leerdoelen kan aantonen. Hij heeft gezegd dat ik het bestand moest doorsturen en dat hij het morgen voor mij gereed heeft.

Vervolgens ben ik met Mohsen aan de slag gegaan met de lander voor Raak. Hij heeft gevraagd of ik hier een blik op wil werpen en wil kijken naar de mobiele versie. Ik heb daarom een paar kleine visuele aanpassingen gedaan om het kleurrijker te maken en ik heb een mobiele versie gemaakt van de testimonials en het contactformulier. Dit heb ik vervolgens via Slack naar Mohsen gestuurd.

Het laatste half uur van de dag heb ik gewerkt aan mijn blog.

8 uur gewerkt vandaag.

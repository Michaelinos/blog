---
title: DUI11 + Nieuwe versie BE
date: 2019-11-01
---

In de ochtend ben ik bezig geweest met de daily UI van de flash message. Eerder deze week heb ik schetsen gemaakt, het was nu tijd om deze digitaal uit te werken in Illustrator. Ik had moeite met het maken van mooie vormen. Ik heb daarom aan Mohsen gevraagd hoe je dit het beste kan aanpakken. Hij zei dat het helpt om te denken in vormen. Ovalen en rechthoeken gebruiken om dingen uit te knippen en uit te werken. Niet uit de losse pols maken. Ook adviseerde hij om een voorbeeld plaatje op te zoeken en dit te gebruiken voor het verkrijgen van een goed perspectief. Dit advies heeft me goed op weggeholpen.

Uiteindelijk is het gelukt om een rechtstaand en omgevallen beker te maken. Deze heb ik aan Mohsen laten zien. Hij zei dat ik het naar een hoger niveau kan tillen door schaduwen en diepte in de illustratie te maken. Hij heeft laten zien hoe dit ongeveer werkt waardoor ik een nog betere illustratie heb gemaakt waar ik tevreden mee ben.
In de middag heb ik gewerkt aan een nieuwe versie voor het BE project. Ik wil nu de kabels van het moodboard in een nieuwe versie verwerken. Ik ben bezig geweest om hier tientallen versies van te maken. Hier heb ik dan ook de rest van de dag mee gestoeid. In het weekend wil ik deze nieuwe versie verder afmaken.

8 uur gewerkt vandaag, 40 uur totaal.

---
title: Onderzoek AH persona's
date: 2020-01-06
---

Na twee weken afwezigheden ben ik weer terug op stage. Dit is de laatste week die ik nog heb voor de deadline van mijn stageverslag. Ten eerste heb ik samen met Rick mijn weekplanning gemaakt. Voor woensdagochtend wil ik de eerste versie van mijn verslag af hebben. Vervolgens kan Rick dit van feedback voorzien zodat ik de rest van de week de tijd heb om deze feedback te verwerken en het eindresultaat in InDesign te zetten.

Er zijn nog een paar werkzaamheden die ik moet doen voordat ik alles heb voor het verslag. Ten eerste heb ik twee weken terug een onderzoek gedaan naar ethiek & Albert Heijn. In de ochtend ben ik bezig geweest om deze onderzoeksresultaten te verwerken in een document. De belangrijkste inzichten heb ik op een rijtje gezet.

A.d.h.v dit document heb ik vier persona’s opgesteld:
-	Anita de huisvrouw
-	Floris de jonge kritische student
-	Petra de korting kanjer
-	Sjonnie de digisceptische consument

Met deze vier richtinge ben ik naar Rick gegaan. Hij vond ze prima aangezien het vier diverse persona’s zijn. Iemand met lage IT kennis die het niet boeit, iemand met lage IT kennis die het wel boeit, iemand met hoge IT kennis die het wel boeit en iemand met hoge IT kennis die het niet boeit. Daarom heb ik besloten de eerste versie verder uit te werken: Anita de huisvrouw. Hiervoor heb ik haar demografische gegevens, drijfveren, biografie en relatie met de Albert Heijn beschreven.

Rick gaf hier als feedback:
-	Je vertelt nu twee keer hetzelfde met de biografie en relatie met de Albert Heijn. Combineer dit in één verhaal.
-	Een biografie moet emotie tonen. Wat voelt Anita? Wat frustreert haar op het gebied van privacy, boodschappen en wat zijn haar verlangens?
-	Voeg nog een stukje toe met ontwerpcriteria. Hoe kan je Anita’s ervaring met de Albert Heijn verbeteren?

We hebben afgesproken dat ik de huidige persona’s laat zoals die zijn en de feedback meeneem naar de volgende versie. De tweede versie met Floris was al een stuk beter. Rick gaf hier als feedback:
-	 Je biografie gaat de goede kant op. Probeer nu een verhaal te vertalen waar emotie bij te pas komt. Laat zien hoe Floris over een situatie denkt, waarom frustreert hem iets? Waarom is hij blij om iets?
-	Koppel de ontwerpcriteria met de biografie. Waarom verlangt Floris deze criteria van een privacy beleid? Beargumenteer je keuzes.

Deze feedback heb ik meegenomen naar het persona van Petra en Sjonnie die Rick heeft goedgekeurd.

9 uur gewerkt vandaag.

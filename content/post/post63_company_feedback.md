---
title: Company update + feedback onderzoek HCD verwerken
date: 2019-11-18
---

Ochtend begonnen met de keek op de week en projectoverleg. Vlak daarna hadden we een company update over de toekomst van RAAK. Hier werd het een en ander uitgelegd over de plannen van RAAK op zowel korte en lange termijn. Hierna ben ik bezig geweest om eerdergenoemde feedback van Rick op mijn HCD onderzoek te verwerken. Rond 13u moest ik weg i.v.m. een afspraak bij het ziekenhuis.

4 uur gewerkt vandaag.

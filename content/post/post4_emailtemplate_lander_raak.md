---
title: Emailtemplate lander RAAK
date: 2019-09-05
---

Dag begonnen met standup meeting. Kort vertelt wat ik gisteren heb gedaan. Vervolgens gelijk een vergadering gehad met het designteam. Hier hebben we het gehad over het werk dat Mohsen heeft gedaan voor de lander. Alex was tevreden met het resultaat en heeft wat taken verdeeld onder Fabian en Mohsen. Ik heb vervolgens aan Fabian gevraagd of hij iets had voor mij te helpen. Hij heeft mij vertelt dat ik een template kan maken voor de email die naar klanten wordt verstuurd vooraf de landerpagina.

Ik heb daarom de vorige emailtemplates van RAAK erbij gepakt en ik heb op het internet gezocht naar inspiratie. Daarna heb ik op papier een kladversie gemaakt om alle informatie te ordenen. Vervolgens ben ik aan de slag gegaan in Adobe XD voor een eerste versie. Na de lunch had ik dit af en aan Fabian laten zien voor feedback:

-	Het is nu nog iets te tekstueel. De grote stukken tekst kunnen beter bewaard blijven voor de lander zelf
-	Probeer er een chronologisch verhaal van te maken. De uitleg is nu iets te abrupt: bijv voorstellen aan lezer > wat wij bieden > hoe krijgen ze meer info
-	Maak er een soort campagne mail

Hier ben ik mee aan de slag gegaan voor een tweede versie. Weer feedback gevraagd:

-	De copy is duidelijk, het is nu een lopend verhaal
-	De bodytekst is nu nog een beetje saai, probeer het wat meer te op te maken. Fabian heeft vervolgens zelf een paar aanpassingen gemaakt om te laten zien wat hij bedoelt.

Fabian was er nu tevreden mee en ik heb het daarom via Slack gedeeld met de rest van het designteam.

Vervolgens kwam Rick naar mij toe en vertelde dat hij het stagebedrijvingsformulier helemaal had ingevuld. Ik heb het doorgenomen en laten ondertekenen door Rick. Tevens heb ik alle andere documenten en mails afgehandeld van school en aan mijn blog gewerkt.

8 uur gewerkt vandaag.

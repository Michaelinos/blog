---
title: Stageplan + feedback verwerking BE + moodboard + stageoverleg
date: 2019-10-31
---

In de ochtend heb ik de laatste dingen afgemaakt voor het stageplan. Ik heb de bedrijfsfeedback verwerkt en hier een reflectie op geschreven. Tevens ook leerdoel 7 en 8 verder afgemaakt.

Gisterenavond heb ik van de opdrachtgever van Betlej Elektrotechniek feedback gekregen op de twee verschillende versies die ik heb gemaakt. De feedback was niet al te best:
-	Het is te druk
-	Voelt goedkoop door de gele kleur
-	Site vertelt niet gelijk wat het biedt
-	Liever geen contactformulier

Dit kwam wel even als een klap binnen maar ik ben er mee aan de slag gegaan om een betere versie te maken. Ik heb daarom ten eerste feedback gevraagd bij Mohsen. Hij gaf aan dat de opdrachtgever waarschijnlijk een serieuzere site wilt. Niet allerlei willekeurige vormen toevoegen aan het ontwerp.
Van Fabian heb ik de volgende feedback gekregen:
-	Eerste opzet goed
-	Vormen voelen nu heel random, geef er betekenis aan. Bijvoorbeeld door associaties van elektrotechniek te koppelen aan de beeldtaal (momenteel voelt het als ruis voor je broer omdat er geen koppel achter zit)
-	USP's kloppen nog niet met de beeldtaal. Gebruik icons of goede foto's die de USP al communiceert.
-	Voeg testimonials toe
-	Je tweede opzet heeft wat meer karakter, communiceer dat ook bij de andere versie
-	Bij recente projecten: voeg titels groot toe. Koppel het aan bedrijven
-	Nog 1 extra vinkje bij contactform

Tevens ook de feedback van Rick genoteerd tijdens de voortgangspresentatie:
-	Mail ons knop toevoegen ipv mailadres
-	Recente projecten anders weergen (kop lijkt nu bij project te horen)
-	Logo beeld meer verwerken in hero
-	Taalgebruik contact gelijktrekken
-	Betaalbaarheid meer nar boven komen
-	Diensten weergave. Voor meer neem contact

Op aanraden van Fabian ben ik begonnen met het maken van een moodboard om duidelijk te hebben waar Betlej Elektrotechniek voor staat. Als eerste heb ik een moodboard gemaakt met de verschillende klanten die BE bedient. Hierdoor kreeg ik een beeld over de vormgeving van de producten die BE bied en voor wat voor klanten BE werkt. Ik had hierdoor nog niet echt het gevoel dat ik iets had waarmee ik verder kan.

Daarom heb ik aan Mohsen advies gevraagd. Hij zei dat ik het beste ook een moodboard kan maken met de stijlrichting en kleuren die mij aanspreken voor het ontwerp. Dit gaf Mohsen en mij het idee om iets te doen met kabels die objecten op de website verbinden. Als vervolgstap heb ik daarom een derde moodboard gemaakt. Deze is volledig gefocust op kabelarchitectuur. Door het maken van dit moodboard is de inspiratie weer aangewakkerd bij mij.

Vlak hierna was het alweer tijd voor het stageoverleg. Hier heb ik vragen over het ontwerpverslag besproken en hoe ik verder moet met Betlej Elektrotechniek. Rick heeft hier uitgelegd hoe ik het beste een ontwerp kan presenteren. Je moet niet uitleggen wat je op het scherm hebt staan, maar onderbouwen waarom je bepaalde keuzes hebt gemaakt.

8 uur gewerkt vandaag.

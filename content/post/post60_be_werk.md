---
title: Projectenpagina + mobiele versie BE
date: 2019-11-13
---

In de ochtend ben ik begonnen met mijn Trello compleet bij te werken voor het Betlej project. Hiervoor heb ik niet meer relevenate taken geschrapt en extra taken toegevoegd voor de homepage en het designsysteem.

Vervolgens weer verder gegaan met het ontwerp voor BE. Ik heb het overzicht voor de projecten en de projectpagina zelf vormgegeven. Dit heb ik vervolgens geprototyped zodat je overal makkelijk doorheen kan klikken. Dit heb ik aan Mohsen laten zien en hij kon er zo snel niet iets op aan merken.

Na de lunch heb ik onderzoek gedaan naar wat een designsysteem precies inhoud. Hiervoor heb ik verschillende artikelen geraadpleegd:

https://uxdesign.cc/everything-you-need-to-know-about-design-systems-54b109851969
https://www.invisionapp.com/inside-design/guide-to-design-systems/
https://superfriendlydesign.systems/glossary/
https://uxdesign.cc/what-the-heck-is-a-design-system-c89a8ea73b0d

Vervolgens ben ik van start gegaan met het maken van een styleguide, wat onderdeel is van een designsysteem. Bij Mohsen had ik hierover wat gevraagd en vervolgens zei hij dat ik beter eerst de mobiele versie kan maken voordat ik met deze styleguide verder ga. Ik heb besloten dit advies op te volgen dus ben ik van start gegaan met de mobiele versie van de website. Hiervoor heb ik verschillende versies van het voorblad gemaakt en het menu geprototyped.

8 uur gewerkt vandaag.

---
title: Enquête AH uitgewerkt + ontwerppresentatie
date: 2019-12-13
---

Voor de laatste keer heb ik Marieke de enquête laten zien. Ze vindt hem nu helemaal prima. Daarom besloten de vragenlijst te verspreiden via social media. De rest van de dag gewerkt aan de locatietracker voor DUI20. Voor de eerste opzet heb ik beschreven wat de belangen zijn van de twee beschreven persona’s. Ter inspiratie heb ik gekeken naar het ‘social credit system’ van China. Vervolgens heb ik een functielijst uitgewerkt met wat er in de app moet komen:
-	Goed gedrag speelt nieuwe beloningen vrij
-	Een ranglijst met de score van de bewoners
-	Een kaart die vertelt wanneer je binnen 500m staat van iemand die schulden heeft/lage score
-	Map die altijd de locatie van de gebruiker trackt
-	Profiel waar je je eigen gegevens kan zien
-	Mogelijkheid om andere gebruikers op te zoeken

Dit heb ik uitgewerkt in een eerste versie wat ik aan Mohsen heb laten zien. Hij gaf als feedback:
-	Het is waarschijnlijk interessanter om een app te maken die subtiel gegevens verzameld.
-	Maak een app die mensen willen gebruiken ipv een gevangenis.

Vervolgens hebben we gebrainstormd en kwamen we op een gecentraliseerd bankensysteem dat vanaf nu door de overheid beheerd wordt. Men moet deze app gebruiken als ze digitale betalingen willen doen. De locatie van de gebruiker wordt gebruikt om dichtbij zijnde oplaadpunten te zien. De app heeft de volgende functies:
-	Geld ophalen met een kaart
-	Face ID/Touch ID voor het inloggen
-	Transactiegeschiedenis
-	Zogenaamde keuzevrijheid maar alles wordt op 1 centraal punt verzameld
-	Onder het mom van gemak geadverteerd
-	Geld tussen elkaar verzenden

De rest van de dag bezig geweest om hier een eerste opzet voor te maken.

8 uur gewerkt vandaag, 39 uur totaal deze week.

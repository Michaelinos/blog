---
title: Presentatie prep + terugkomdag
date: 2019-12-10
---

Gisterenavond heb ik feedback gekregen op mijn concept stageverslag. In de ochtend ben ik voornamelijk bezig geweest om deze feedback uit te werken in een handig document dat ik kan gebruiken ter referentie wanneer ik verder ga werken aan mijn stageverslag.

Toen ik hier klaar mee was heb ik een begin gemaakt aan een nieuwe versie van de ontwerppresentatie die ik donderdag ga presenteren. Hiervoor heb ik nieuwe screenshots verzameld en deze op zo’n manier in PowerPoint gezet dat het lijkt alsof je scrolt op een webpagina. In de middag had ik een terugkomdag op school.

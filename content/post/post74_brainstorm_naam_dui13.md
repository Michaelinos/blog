---
title: Brainstormen naam + DUI13
date: 2019-12-03
---

Bij het vorige stageoverleg heb ik met Rick afgesproken dat ik vanaf nu aan het begin van de week met Rick mijn weekplanning doorneem. Hier ben ik dan ook de dag mee begonnen.
Kort hierna werd door Rick aan mij verzocht om een aantal potentiele namen te verzinnen, ter voorbereiding van de creatieve sessie voor Bomenbanen. Rond 09:30 was het tijd om deze sessie te doen met Rick en Martin. De creatieve techniek die we hebben gebruikt was longlist & shortlist. Na een grote naam dump hebben we uiteindelijk gekozen voor drie namen:
-	Bomen Academie (saai maar veilig)
-	De Groen Meesters (speelt in op stoer zijn en vakmanschap)
-	Stam (naamspeling op boomstam en groepering, geeft gevoel van samenhorigheid)

Na deze sessie ben ik verder gaan werken aan het prototype voor DUI13. Alle schermen zijn nu volledig af en het prototype is volledig klikbaar gemaakt.

8 uur gewerkt vandaag.

---
title: Vooronderzoek AH + DUI13
date: 2019-11-29
---

In de ochtend heb ik een soort vooronderzoek gedaan voor ethiek binnen het privacy beleid van de Albert Heijn. Als eerste heb ik gekeken naar ethiek binnen ontwerp. Daarvoor heb ik brononderzoek gedaan naar wat ethisch ontwerp is, hoe dark patterns worden verwerkt in ontwerpen en hoe ethiek steeds belangrijker wordt in de sector.

Vervolgens heb ik kritisch gekeken naar de vormgeving van het privacy beleid van AH, Jumbo, Spar, Apple en Google. Hier een aantal foto’s van gemaakt en gegevens verzameld. Ook heb ik bij de Albert Heijn heel het proces van account aanmaken en een bonuskaart aanvragen gevraagd en hier screenshots van gemaakt. Opvallend was dat nergens in het proces expliciet om toestemming wordt gevraagd over gegevensverzameling en je dit ook nergens uit kan zetten. Ik weet alleen nog niet zo goed welke kant ik op wil gaan met dit onderzoek, dit wil ik volgende week met Rick bespreken.

In de middag heb ik weer verder gewerkt aan daily UI 13. De Runescape direct messaging app. Ik heb het geheel compacter gemaakt en schermen uitgewerkt voor de ingame auction house.

8 uur gewerkt vandaag, 40 uur totaal deze week.

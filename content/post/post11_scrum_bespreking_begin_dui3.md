---
title: Scrum bespreking + begin DUI3
date: 2019-09-13
---

In de ochtend Scrum documentatie doorgenomen die mij is aangeleverd door Mike. In dit document werd heel uitgebreid uitgelegd hoe Scrum werkt en voor wat het wordt gebruikt. Hier ben ik eigenlijk heel de ochtend mee bezig geweest.

In de middag heb ik een start gemaakt aan DUI3: het maken van een lander pagina. Hierbij heb ik gekozen voor een Zuid-Amerikaanse cocktailbar. Tevens wilde ik ook een logo maken voor deze bar, omdat ik hier nog niet zo goed in ben. Ik wist niet zo goed hoe ik aan deze opdracht moest beginnen, dus ik heb aan Fabian om hulp gevraagd. Hij heeft mij het volgende vertelt:

-	Verzin hoofdonderwerp
-	Verzamel dingen die je wilt presenteren/verkopen aan de klant
-	Een gevoel dat je wilt meegeven (premium? Cheap?)
-	Inspiratie opzoeken
-	Moodboard maken dat aan die criteria voldoet
-	De rest komt daarna (meestal) vanzelf

Dit heeft me goed op weg geholpen om een eerste opzet te maken. Ik ben daarom verder gegaan met het Zuid-Amerikaanse thema, heb hier een moodboard voor gemaakt en heb besloten om de zon te gebruiken voor het logo, een belangrijke god bij Inca religie.

Rond 14.00u had ik een afspraak met Mike over de werking van Scrum. Hier heeft hij mij kort en bondig uitgelegd wat Scrum inhoudt en hoe het gebruikt wordt bij RAAK. Hier ben ik veel te weten gekomen, maar dit is te veel om hier te beschrijven. Daarom wil ik hier een apart verslag over schrijven. Ook heb ik met Mike afgesproken dat hij mij gaat betrekken bij de planning van het WeConnekt project.

8 uur gewerkt vandaag, 40 uur totaal deze week.

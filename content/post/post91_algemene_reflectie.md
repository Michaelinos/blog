---
title: Algemene reflectie stageperiode
date: 2020-01-10
---

Ten eerste was ik erg blij dat ik aangenomen was. Na lang zoeken en veel solliciteren had ik eindelijk een plek gevonden waar ik een goed gevoel bij had. Bij RAAK heerst een relaxte werksfeer. Er wordt hard gewerkt maar dit wordt gecombineerd met leuke uitjes en tussendoortjes. Een groot pluspunt was de verzorgde lunch, eindelijk geen brood meer smeren!

Bij RAAK werkte ik als UX-designer. Dit betekende dat ik meewerkte aan verschillende design challenges voor diverse klanten. Wanneer ik niets te doen had werkte ik aan Daily UI challenges. 1 of 2x in de week werkte ik een nieuwe uitdaging uit. Ik heb regelmatig feedback gevraagd bij collega’s Fabian en Mohsen (UX-designers) en Rick mijn stagebegeleider. Ik merkte dat iedereen ontzettend kritisch was op mijn werk, er werd tot in detail goed onderbouwende feedback gegeven. Dit vond ik ontzettend fijn want hierdoor heb ik veel kunnen leren en continu mijn kennis kunnen verbreden. Ik dacht dat ik al het een en ander wist maar dit heeft mij doen realiseren dat ik nog veel te leren heb.

Tijdens mijn stageperiode had RAAK een rustige periode. Er waren niet veel projecten waardoor ik niet vaak druk heb ervaren. Aan de ene kant vond ik het jammer dat ik niet vaak dingen heb kunnen maken voor echte klanten. Een groot voordeel hiervan was dat ik de ruimte had om mijn week helemaal in te richten met dingen zie ik zelf interessant vind om te leren. Ik heb me verdiept in vakliteratuur, een eigen animatie gemaakt, veel Daily UI’s gemaakt en een eigen project opgezet en volledig doorlopen via de scrum methodiek. Uiteraard heb ik op al deze werkzaamheden uitgebreide feedback en begeleiding gehad.

 
Door Rick heb ik geleerd om aan het begin van de week een planning te maken en hier uren aan te koppelen. Hierdoor wist ik wat mij die week te wachten stond en kon ik makkelijker ja of nee zeggen als iemand mijn hulp nodig had. Door de bedrijfsfeedback en stageoverleggen met Rick heb ik een groot inzicht verkregen wat mij goed heeft geholpen met mijn opdrachten achterliggende context geven. Een ander perspectief als uitgangspunt nemen voor ontwerpbeslissingen, persona’s maken, scherp hebben wat ik wil bereiken en voor wie ik wat maak. Zo krijg je waardevolle ontwerpen.

Ik zie dat ik kritischer ben geworden, meer zelfverzekerd, beter kan ontwerpen en veel bewuster nadenk over het waarom bij de opdrachten. Ik realiseer nu ook dat ik UX-design erg leuk vind om te doen en iedere dag met plezier naar stage ga.
Ik merk ook dat ik in mijn vrije tijd heel erg op de UX ben gaan letten in games/websites/apps (wat af en toe vervelend kan zijn).

Enkele hoogtepunten van mij waren:
-	De PeckaKucha presentatie. Ik had er nog nooit van gehoord en ik vond het een hele leuke presentatie om te doen.
-	Voor Betlej Elektrotechniek een eigen scrumproject doorlopen, wat een hele uitdaging was. Uiteindelijk is hier een mooie website uit gekomen waar de opdrachtgever tevreden over is. Het was ook tof om mijn het einderesultaat te laten zien door middel van een ontwerppresentatie.
-	Het bezoek aan Dutch Design Week. Ik was hier nog niet eerder geweest, fascinerend om te zien waar de designwereld nu mee bezig is.
-	De creatieve sessie voor Bomenbanen. Ik vond het interessant om mee te gaan naar het bedrijf en ook erg leuk dat ik een nieuwe naam heb kunnen verzinnen voor hun die zij wellicht kiezen.

Na de afronding van mijn stage wil ik mij focussen op mijn portfolio. Ik heb veel opdrachten gemaakt die ik mooi kan presenteren op mijn website. Ik zit er ook aan te denken om te solliciteren bij een groot ontwerpbureau. Mijn voorkeur gaat nog altijd naar kleine ontwerpbureau’s, maar ik wil zelf eens ervaren hoe het is om stage te lopen bij een grote organisatie. Tevens wil ik waarschijnlijk een minor gaan volgen in branding/marketing. Ik heb hier nog geen ervaring mee dus lijkt het mij interessant om mij hierin te verdiepen. En wat ik nog wil meenemen van mijn laatste bedrijfsmoment is:
-	Empathie verbeteren door dit als vertrekpunt te gebruiken bij een project
-	Meer mijn stem laten horen in een groep
-	Filters uitzetten tijdens ideevorming
-	Een lijstje maken met ontwerpidolen die mij aanspreken

Om af te sluiten: Ik heb een ontzettend leuke en leerzame stageperiode gehad.

---
title: Onderzoek AH
date: 2019-12-06
---

Vandaag beziggehouden met mijn onderzoek naar de AH. De taken die ik voor mijzelf heb opgesteld zijn:
-	Inleiding schrijven
-	Hoe definieer ik ethiek in dit onderzoek
-	Research doen naar wat dark patterns zijn
-	Wat zijn voorbeelden van dark patterns
-	Wat wordt de structuur van dit onderzoek

Hier ben ik de rest van de dag mee bezig geweest.

8 uur gewerkt vandaag, 36 uur totaal deze week.

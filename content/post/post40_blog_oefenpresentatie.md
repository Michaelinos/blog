---
title: Blog bijwerken + oefenpresentatie PeckaKucha
date: 2019-10-14
---

Ik liep weer een beetje achter met mijn blog. In de ochtend ben ik bezig geweest met deze weer volledig bij te werken. Rond half 12 had ik een oefenpresentatie met Rick. In het weekend had ik hier een PechaKucha voor voorbereid. Ik heb als feedback ontvangen:
-	Vermijd schuttingtaal zoals beetje, wel, misschien. Zo komt je presentatie niet zelfverzekerd over.
-	Zorg voor meer samenhang tussen wat je laat zien en wat je presenteert.
-	De presentatie gaat over jou. Dus maak het persoonlijker, betrek jezelf meer met wat je op het scherm laat zien.
-	Verschillende kleine inhoudelijke punten over welke plaatjes je beter kan laten zien.

De rest van de dag gewerkt aan bovenstaande feedback te verwerken in een nieuwe versie van mijn presentatie.

8 uur gewerkt vandaag.

 

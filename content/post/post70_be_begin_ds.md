---
title: Verdere uitwerking BE + begin design system
date: 2019-11-27
---

Vandaag heb ik weer verder gewerkt aan het Betlej Elektrotechniek project. Zo heb ik ten eerste Trello bijgwerkt. Alle taken netjes opgeruimd en aan een nieuwe sprint begonnen. Vervolgens heb ik een paar kleine verbeteringen gemaakt aan de mobiele versie, waardoor de sectie over diensten overzichtelijker is geworden en je sneller overal doorheen kan gaan.

Nadat de mobiele versie helemaal compleet was, heb ik verder gewerkt aan de tabletversie van het ontwerp. Aan het ontwerp is weinig veranderd, in principe is het een compactere versie van het desktopontwerp geworden. Toen ik hier klaar mee was, was het alweer eind middag. Ter voorbereiding voor morgen heb ik extra onderzoek gedaan naar wat een design system precies inhoudt en welke aspecten voor mij handig zijn om uit te werken.
Zo kwam ik uiteindelijk tot onderstaande lijst:
-	Principes
o	Doel van het bedrijf
o	Kernwaarden
o	Doel website
o	Strategie
-	Merk identiteit & taal
o	Kleuren
o	Fonts
o	Foto’s
o	Taal & toon
o	Iconen
o	Vormen
-	Components & patterns
-	Best practices

Deze lijst heb ik vervolgens verder gestructureerd in Adobe XD.

8 uur gewerkt vandaag.

---
title: DUI9 + DUI10 + voorbereiding voortgangspresentatie
date: 2019-10-28
---

Na de keek op de week en projectoverleg ben ik aan de slag gegaan met de daily UI challenges. Als eerst ben ik verder gegaan met het maken van een CarPlay Spotify. Ik heb hiervoor gestures toegevoegd om een nummer te liken en om een afspeellijst te shuffelen.

Nadat ik deze challenge had afgerond ben ik verder gegaan met de volgende challenge. Het maken van een social share icon waarbij interactie mogelijk is om iets te delen op bijvoorbeeld facebook of twitter. Hiervoor heb ik in eerste instantie een artikeltje uitgewerkt met aan de onderkant de mogelijkheid om een artikel te delen.

Net voor de middag kwam Rick naar mij toe met de vraag wat de status is van mijn project en of ik de voortgang wil presenteren in de vorm van een voortgangspresentatie. Daarom heb ik een beknopte presentatie gemaakt met hoe het volgen van de planning is verlopen, waar ik tegen aan liep en wat ik denk dat de oorzaken en vervolgstappen zijn. Ook heb ik een Excelsheet gemaakt om mijn voortgang visueel te kunnen onderbouwen.

Tevens heb ik een tweede versie uitgewerkt voor het Betlej Elektrotechniek project. Deze versie heb ik veel speelser gemaakt met ronde vormen.

8 uur gewerkt vandaag.

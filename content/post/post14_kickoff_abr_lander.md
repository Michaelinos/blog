---
title: Kickoff ABR lander
date: 2019-09-18
---

Ik ben de dag begonnen met de launch voor ABR (Antibiotica resistentie campagne). Voor de organisatie achter deze campagne heeft RAAK in het verleden een website geleverd. Nu is er gevraagd om voor de ‘nationale handen was dag’ deze campagne nieuw leven in te blazen om zoveel mogelijk aanmelden te krijgen. De wireframes en copy tekst was al aanwezig. Nu was de vraag aan mij en de andere designers om een lander te maken voor deze campagne. Samen met Mohsen hebben wij allebei drie richtingen uitgewerkt voor de header. Mijn eigen interpretaties heb ik uitgewerkt op basis van de bestaande foto’s en styleguide.

Om 14.00u had ik weer een overleg met de rest van het designteam voor een tussentijdse update. Hier heeft iedereen zijn ontwerpen laten zien. Uiteindelijk hebben we besloten om met de versie van Mohsen verder te gaan, dit vond ik ook de beste uitwerking.

Nu was mijn taak om voor vier verschillende thema’s foto’s te verzamelen en om een modal uit te werken voor de aanmeldbevestiging. Met de foto’s ben ik begonnen. Hiervoor heb ik voor elk thema een moodboard gemaakt en dit met de rest gedeeld. Voor de modal ben ik begonnen met een eerste opzet. Hiervoor heb ik gebruik gemaakt van de bestaande copy. Ook heb ik het geheel wat speelser gemaakt door een illustratie toe te voegen.
Vervolgens heb ik bij Fabian feedback gevraagd:
-	Belangrijke tekst mag groter gemaakt worden
-	Emailadres moet dikker gedrukt worden en niet het uiterlijk van een knop
-	Nog een knop toevoegen voor leesbevestiging
-	Misschien i.p.v. vuurwerk bubbels gebruiken in de illustratie

Deze feedback heb ik dan ook verder verwerkt in de modal. Ook heb ik nog feedback gevraagd bij Mohsen, hij zei dat het ook mooi is om de illustratie verschillende kleurtjes te geven. Dit heb ik dan ook toegepast. Rond dit moment was het alweer half 6 en ben ik gaan afsluiten.

8 uur gewerkt vandaag.

---
title: Bedrijfsfeedback 2 + stageverslag
date: 2020-01-09
---

In de ochtend na de daily heb ik samen met Rick bedrijfsfeedback 2 gedaan. Om het kort te zeggen, Rick was erg tevreden over mij en heeft goede feedback gegeven waar ik mee verder kan. De belangrijkste punten waar ik aan moet werken zijn:
- Empathie verbeteren door dit als vertrekpunt te gebruiken bij een project
- Meer mijn stem laten horen in een groep
- Filters uitzetten tijdens ideevorming
- Een lijstje maken met ontwerpidolen die mij aanspreken

Kort hierna was ik even weg voor een bezoek aan de orthodontist. Na dit bezoek heb ik verder gewerkt aan de feedback die ik op mijn verslag heb gekregen. Toen dit eenmaal klaar was heb ik al mijn leerdoelen in InDesign gezet en naar Rick doorgestuurd voor feedback. In de tussentijd heb ik al mijn werk geëxporteerd en in mappen geordend zodat dit makkelijk in InDesign verwerkt kan worden. Dit heeft de rest van mijn dag in beslag genomen.

8 uur gewerkt vandaag.

---
title: Uitwerking modal WeConnekt
date: 2019-09-16
---

Dag begonnen met keek op de week. Hier zijn de voortgang met projecten besproken en gepraat over een mogelijk emailtemplate waar ik aan ga werken voor de nieuwsbrief van RAAK.

Kort hierna een design meeting gehad. Hier de opdracht van Fabian gekregen om te werken aan nieuwe schermen voor de WeConnekt app. Fonts aangeleverd gekregen. Eerste opdracht was om een popup modal uit te werken in de nieuwe huisstijl. Allereerst even opgezocht hoe ik een modal het beste kan ontwerpen. Bij dit artikel gekomen voor richtlijnen: https://uxplanet.org/best-practices-for-modals-overlays-dialog-windows-c00c66cddd8c. Tevens ook toegang gevraagd naar bestaande WeConnekt omgeving zodat ik kan kijken wat er inhoudelijk allemaal in moet komen.

Na een tijdje had ik een eerste versie uitgewerkt. Aan Fabian hierop feedback gevraagd. Was prima, nog alleen als feedback om alle hoeken gelijk uit te lijnen bij het venster. Vervolgens als opdracht gekregen om nu de instellingen en ‘available corridors’ scherm verder uit te werken. Hier ben ik de rest van de dag dan ook mee bezig geweest.

8 uur gewerkt vandaag.

---
title: Weconnekt (met feedback) + logo DUI3
date: 2019-09-17
---

Dag begonnen met een standup meeting. Vertelt waar ik gisteren mee bezig geweest en dat ik vandaag weer aan de slag ga met de WeConnekt app. Ik heb daarom een eerste versie van de instellingen, available corridors en profiel gemaakt. Hierop heb ik feedback gevraagd bij Rick. Hij gaf als feedback:
-	Scherm voor profiel bijwerken kan beter apart ipv een modal. Het is te veel informatie voor een modal en je hebt de kans dat je perongeluk verkeerd klikt, het afsluit en weer opnieuw moet beginnen.
-	Instellingen verder volledig uitwerken. Pijltjes moeten aangeven dat je doorschuift in het instellingen scherm.
-	Available corridors scherm aanvullen met info icoon. Ook add volumeknop prominenter aanwezig maken sinds je wilt dat de gebruiker daarop gaat klikken.
-	Miscshien kijken of je add volume in 1 scherm kan uitwerken zodat de gebruiker niet te veel handelingen hoeft te verrichten.
-	Overal terug knoppen neerzetten.
-	Onderste balk aanvullen met country/corridor toevoegen ipv alleen een + icon voor verschillende acties.
-	Locatie kaart aanpassen op map

Weer goede feedback ontvangen waar ik mee aan de slag kan dus. Ik ben daarom aan de slag gegaan om deze feedback in mijn ontwerpen te verwerken. Voor wat afwisseling ben ik ook aan de slag gegaan met mijn huidige daily ui challenge. Nu was de uitdaging om een logo uit te werken. Hiervoor wil ik de zon gebruiken als uitgangspunt. Ik heb daarom wat inspiratie opgezocht wat betreft logo ontwerp en veel schetsen uitgewerkt op papier. Toen ik eenmaal wat had ben ik dit in Illustrator verder gaan uitwerken. Uiteindelijk ben ik niet helemaal tevreden met hoe het is geworden maar ik heb wel nieuwe dingen geleerd op het gebied van het ontwerpproces.

8 uur gewerkt vandaag.

---
title: Logo ontwerp Bomenbanen
date: 2019-12-04
---

Rick heeft gisteren een verhaal en uitstraling verzonnen bij de namen die gisteren zijn gekozen:
-	Bomen Academie (saai maar veilig)
-	De Groen Meesters (speelt in op stoer zijn en vakmanschap)
-	Stam (naamspeling op boomstam en groepering, geeft gevoel van samenhorigheid)

Deze ochtend zijn Mohsen en ik door Rick gebrieft om logo’s te ontwerpen voor Stam en De Groen Meesters. Hij heeft geadviseerd om te beginnen met het maken van een moodboard. Na de briefing ben ik hier gelijk mee aan de slag gegaan. Als eerste had ik een moodboard gemaakt met foto’s die te maken hebben met bosbeheer. Dit was niet helemaal de bedoeling. De foto’s hadden verschillende stijlen die niet helemaal bij elkaar passen. Ik moest eigenlijk een moodboard maken die een bepaald stijlbekeld geven. Dit inzicht heeft het volgende moodboard een stuk makkelijker gemaakt. Vervolgens heb ik twee moodboards gemaakt, 1tje met de focus op luxe en exclusiviteit en 1tje met de nadruk op badges en militaire ranks. Deze waren nu goed waardoor ik kon beginnen met het logo ontwerp. Uiteindelijk ben ik aan het van de dag met drie verschillende proposities gekomen.

8 uur gewerkt vandaag.

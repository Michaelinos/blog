---
title: Gewerkte uren sinds 24 januari
date: 2020-01-24
---

715u totaal gewerkt sinds 24 januari + 16u terugkomdag & 128 uur blog/stageverslag

Week 1 - 2 sept = 40u <br>
Week 2 - 9 sept = 40u <br>
Week 3 - 16 sept = 40u <br>
Week 4 - 23 sept = 40u <br>
Week 5 - 30 sept = 32u (1 dag ziek) <br>
Week 6 - 7 okt = 32u (1 dag ziek) <br>
Week 7 - 14 okt = 40u <br>
Week 8 - 21 okt = 32u (1 dag verlof) <br>
Week 9 - 28 okt = 40u <br>
Week 10 - 4 nov = 40u <br>
Week 11 - 11 nov = 40u <br>
Week 12 - 18 nov = 36u (halve dag verlof) <br>
Week 13 - 25 nov = 40u <br>
Week 14 - 6 dec = 36u <br>
Week 15 - 13 dec = 39u (afspraak tandarts/ortho) <br>
Week 16 - 16 dec = 20u (donderdag operatie) <br>
Week 17 - 23 dec = 16u (twee feestdagen + verlof) <br>
Week 18 - 30 dec = 8u (feestdag + verlof) <br>
Week 19 - 6 jan = 43u <br>
Week 20 - 13 jan = 21u (ziek + verlof) <br>
Week 21 - 20jan = 40u

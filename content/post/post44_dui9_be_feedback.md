---
title: RET + BE Project wireframes
date: 2019-10-22
---

Vandaag ben ik verder gegaan met mijn Daily UI Challenge omtrent een music player. Ik was vorige week in eerste instantie begonnen met een desktop versie, maar ik realiseerde nu eigenlijk dat het gewoon een slecht uitgewerkte Spotify variant aan het worden is. Ik ben daarom opnieuw begonnen en om het interessant te houden heb ik besloten een Spotify versie te maken voor gebruik met Apple CarPlay. Dit leek mij een veel interessantere opdracht omdat je met andere eisen rekening moet houden als je de applicatie in een auto gebruikt. Daarom heb ik een interface gemaakt met zo min mogelijk knoppen, grote aanraakvelden en een herkenbare uitstraling voor CarPlay gebruikers.

In de middag heb ik aan Rick feedback gevraagd op de wireframes die ik vorige week vrijdag voor BE heb uitgewerkt. Ik was voornamelijk nieuwsgierig of deze uitwerking voldoende is om een goede keuze te maken voor vervolgstappen. Rick gaf als feedback dat wireframes nog net iets te vaag is om een goede keuze te maken. Daarom stelde Rick voor om bij alle drie de ontwerpen een uur te reserveren om deze zo ver mogelijk uit te werken. Het resultaat hiervan kan ik a.s. donderdag aan een ontwerper presenteren met de vraag of hij feedback wil geven op wat ik heb gemaakt. Ik heb daarom met Mohsen afgesproken om a.s. donderdag een uur hiervoor te reserveren.
Vervolgens ben ik de rest van de middag aan de slag gegaan om mijn ontwerpen verder uit te werken. Zo heb ik kleuren uitgezocht, fonts uitgetest en foto’s verzameld.
8 uur gewerkt vandaag.

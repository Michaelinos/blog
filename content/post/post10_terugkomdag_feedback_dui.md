---
title: Terugkomdag school + verwerking feedback DUI’s
date: 2019-09-12
---

Vandaag de dag begonnen met een terugkomdag op school. Hier voornamelijk informatie gekregen over het stageplan en ik heb een presentatie gegeven over het bedrijf waar ik stageloop en wat mijn leerdoelen zijn.

Vervolgens gewerkt aan DUI1 en 2 om de feedback van Rick te verwerken. Aan Mike (projectmanager) gevraagd of hij mij kan helpen met het leren van Scrum, voor morgen vergadering hierover ingepland.
Kort daarna begon de borrel van een collega die vertrekt bij RAAK.

8 uur gewerkt vandaag.

---
title: Nieuwe landingpage RAAK
date: 2019-09-03
---

In de ochtend ben ik begonnen met een standup. Hier kan je in het kort vertellen wat je gisteren hebt gedaan en waar je nu mee bezig bent of tegen aan loopt. Vervolgens is Mike, de projectmanager, bij mij langs gekomen om uitleg te geven over de verschillende systemen waar mee gewerkt wordt bij RAAK.

Ook heb ik even met Rick, mijn stagebegeleider, gesproken. We hebben afgesproken om morgen bij elkaar te zitten om al mijn leerdoelen en overig school werk te bespreken.

Vervolgens ben ik met het designteam in een aparte ruimte gaan zitten om te werken aan de huidige design challenge: het maken van een nieuwe landingspagina om klanten te werven. Alex heeft de dag ervoor copy voor ons geschreven die wij kunnen gebruiken om de landingspagina te vullen. Aan ons de taak om deze informatie goed te structureren en presenteren.

We zijn begonnen met het maken van een Google Slide (soort PowerPoint) waarbij we de copy op een logische manier hebben gestructureerd:

-	Hero
-	Video
-	Social Proof
-	CTA
-	Waarom RAAK
-	Features
-	Cases
-	Resultaat
-	CTA

Ik heb hier inbreng in gehad door mee te denken over wat voor copy we gebruiken om de bovenstaande categorieën op te vullen. Vervolgens was aan mij de taak om a.d.h.v. een opzet van Mohsen, de visual designer, de landingspagina op te vullen en structureren met de copy. Na de lunch was het tijd voor de vergadering met het designteam. Hier hebben we met Alex besproken wat we tot nu toe hebben gedaan met de landingspagina.

Na de vergadering ben ik verder aan de slag gegaan met het ontwerpen. Ik heb de landingspagina afgemaakt en gedeeld met de rest van het designteam. Eveneens heeft de rest hun versie gedeeld. Als feedback heb ik ontvangen:

-	Testimonials te abrupt, kan beter ingeleid worden met verhaal
-	Eerste CTA ook abrupt, beter heel klein zodat het niet afdoet van het verhaal
-	Functies/milestones onderbouwen met fotos die boodschap versterken

Deze feedback is vervolgens meegenomen in de versie van Mohsen, die hier verder mee aan de slag is gegaan. Ook heeft Fabian mij een interessant artikelen doorgestuurd waar de gebruikte structuur op is gebaseerd: https://www.julian.com/guide/growth/landing-pages
Deze pagina heb ik aandacht doorgelezen en ik zag mijn gekregen feedback hier ook in terugkomen.

De rest van de dag heb ik gewerkt aan het stageplan en mijn blog.

8 uur gewerkt vandaag.

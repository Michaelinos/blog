---
title: RET + BE Project wireframes
date: 2019-10-18
---

Vandaag in de ochtend verder gewerkt aan RET. Fabian heeft aan mij gevraagd om foto’s te verzamelen van een persoon in het OV die we kunnen gebruiken voor abri poster. Hier ben ik heel de ochtend mee bezig geweest omdat het heel lastig was om een goed geschikte foto te vinden hiervoor.

Tevens hebben Fabian en ik aan de projectmanager en manager ons resultaat gepresenteerd. Hier hebben wij wat kleine feedback gekregen maar over het algemeen waren ze erg tevreden met het eindresultaat.

In de middag heb ik gewerkt aan mijn project voor Betlej Elektrotechniek. Ik had al eerder een wireframe uitgewerkt. Nu heb ik weer allerlei inspiratie opgezocht op Dribbble en Behance om meerdere wireframes uit te werken. Op deze manier kan ik snel meerdere ontwerpen uit proberen zonder dat ik er teveel tijd in stop.

Zo heb ik een ontwerp gemaakt die heel speels is, eentje meer gericht op visueel beeld en eentje waarbij ik alle info en beeld in een blokkenpatroon uitwerk.

8 uur gewerkt vandaag, 40 uur totaal deze week.

---
title: BE project + feedback verwerking
date: 2019-11-06
---

Vandaag weer heel de dag gewerkt aan het BE project. Van het weekend heb ik een opzet gemaakt van een nieuwe versie. Deze heb ik aan Rick laten zien voor feedback:
-	Huisstijl meer teruglaten komen
-	Footer en contactvorm samen zetten met een donker vlak
-	Recenten projecten minder nadrukkelijk aanwezig.
-	Combineer logo's met projecten
-	Testimonials met 9 terug laten komen
-	Spacing shit die ze bieden
-	Logo terugvinden met huisstijl. Meer aanwezig maken

Hierdoor kwam ik op een versie waar ik zelf erg tevreden mee was. Ik heb alle diensten een eigen vak gegeven, de foto’s van projecten gekoppeld aan logo’s van klanten en echte testimonials verwerkt.

Vervolgens heb ik weer feedback aan Rick en Fabian gevraagd:
-	Kabel door verschillende vakjes laten lopen
-	Projecten stuk over full width laten lopen
-	Knoppen alle projecten scheiden
-	9 van beoordeling meer aan laten voelen als prijs/beoordeling (bijv sterren toevoegen)
-	Titels toevoegen aan testimonials
-	Kabel doortrekken tot laatste kastje; kast ook meer laten aanvoelen als kast
-	Subtitel maken van onderste stuk tekst bij contactform
-	Contactform veldjes iets meer aandacht geven

Deze feedback heb ik weer verwerkt. Vanavond ga ik de nieuwe versie weer aan mijn broer laten zien.

8 uur gewerkt vandaag.

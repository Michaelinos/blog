---
title: Uitwerking design system
date: 2019-11-28
---

Als eerste heb ik aan Rick feedback gevraagd op het design system dat ik wil gaan uitwerken. Rick vertelde me dat ik eerst duidelijk moet hebben voor wie ik dit wil gaan maken. Een marketeer en directeur wilt natuurlijk meer weten over waarom bepaalde keuzes zijn gemaakt en welke onderbouwing dit heeft, terwijl een developer gewoon wilt weten welk font en hexcodes hij nodig heeft. Uiteindelijk heb ik besloten dit te schrijven voor mij, als developer die dit project later moet gaan programmeren. Daarom zijn we tot de volgende structuur gekomen:
-	Principes
o	Over Betlej Elektrotechniek
o	Doel website
o	Kernwaarden
-	Merk identiteit & taal
o	Kleurenpalet
o	Typografie
o	Fotografie
o	Iconografie
o	Vormtaal
-	Components & patterns

Overige aspecten van het design system laten we voor nu achterwegen omdat het een redelijk klein project is en het niet zo handig is om allerlei best practices te gaan documenteren als hier toch later niets mee wordt gedaan. In ieder geval heb ik wel nagedacht over wat een design system inhoud, hoe het werkt en hoe ik dit later kan inzitten bij een groter project.

Na dit gesprek ben ik aan de slag gegaan met het uitwerken van het design system. Hier ben ik de rest van de dag mee zoet geweest aangezien er een aantal kleine inconsistenties in mijn ontwerp zaten op het gebied van typografie en iconografie die ik bij alle ontwerpen eruit heb moeten halen. Aan het eind van de middag ook een stageoverleg gehad met Rick.

8 uur gewerkt vandaag.

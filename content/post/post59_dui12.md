---
title: Feedback DUI + DUI12
date: 2019-11-12
---

In de ochtend heb ik de laatste dingen bijgewerkt voor mijn blog. Deze is nu volledig up-to-date. Vervolgens heb ik een aantal daily UI’s naar Rick gestuurd voor feedback. Deze feedback heb ik netjes genoteerd en opgeslagen. Tevens ook de laatste feedback gekregen op mijn onderzoeksrapport. Hier gaf Rick aan dat mijn inzichten momenteel nog uit het perspectief van mij zijn geschreven i.p.v. waardevolle inzichten voor de lezer te noteren.

Feedback music player:
-	Back knop op begin scherm is nutteloos
-	Voortgang in nummer te onduidelijk (laag contract)
-	Pijltje voor gesture staat de verkeerde kant op

Feedback social share:
-	Minimal maar goed! Omdat je toch een fold-out menu gebruikt zou ik de ‘share this’ tekst wel opnemen in de button

Feedback flash message:
-	Gebruik een persistent field of view (zelfde diepte/perspectief)
-	Contrast/readability van groene knop niet goed
-	Design rechter plaatje is off-center
-	Vage message bij error. Kijk naar meer menselijke messaging zoals in de confirmation

Vervolgens verder gewerkt aan mijn huidige daily UI: ontwerp voor een e-commerce shop.
Hierbij heb de ik eerdere feedback van Mohsen verwerkt. Alle onbelangrijke informatie heb ik kleiner gemaakt, bezorginformatie en manier om artikelen op te slaan toegevoegd. Tevens ook gespeeld met de nieuwe animatie opties van Adobe XD.

Aan het eind van de dag ben ik bezig geweest met het verzinnen van een idee voor de nieuwe daily UI challenge: direct messaging app. Ik heb hiervoor wat inspiratie opgezocht op Dribbble, maar zo’n beetje elk ontwerp had dezelfde saaie layout. Daarom kwam ik op het idee om een chat app te maken voor RuneScape (een oude rpg met een jaren 90 middeleeuws ontwerp). Ik heb wat screenshots gemaakt van de layout van het spel en een kleurenpalet gemaakt. Vervolgens heb ik aan Fabian gevraagd hoe ik het beste het pixelachtige ontwerp van RuneScape na kan maken in een eigen ontwerp. Fabian heeft me toen wat voorbeelden laten zien van eigen illustraties die hij heeft gemaakt en mij doorverwezen naar www.pixelatorapp.com om makkelijk illustraties te pixelaten.

8 uur gewerkt vandaag.

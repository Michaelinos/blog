---
title: Eerste stagedag
date: 2019-09-02
---

Vandaag was mijn eerste stagedag, wat natuurlijk erg spannend is. Ik ben deze ochtend goed verwelkomt. Ik kreeg een rondleiding door het kantoor en er werd uitgelegd waar alles staat. Vervolgens zijn we de dag begonnen met een vergadering met alle aanwezige medewerkers. Hier werd kort vertelt waar iedereen mee bezig is en wat er op de planning staat voor vandaag.

Na deze vergadering werd mijn werkplek toegewezen en kreeg ik toegang tot alle benodigde systemen. Hierna was het tijd voor de vergadering met het designteam. Alex (manager) heeft hier vertelt dat RAAK een nieuwe landingspagina wilt ontwikkelen die verstuurd kan worden naar potentiele klanten. Op deze pagina moet duidelijk zijn wat RAAK te bieden heeft en hoe men contact op kan nemen met ons. Hiervoor zijn een aantal taken verdeeld onder het vaste designteam. Toen wij hier mee klaar waren, was het alweer tijd voor de gezamenlijke lunch met het team.

Aangezien Rick, mijn stagebegeleider, niet aanwezig was vandaag, ben ik aan de slag gegaan met opdrachten voor school. Ik had de stagehandleiding nog niet doorgenomen dus ik ben begonnen door deze door te nemen. Ik heb alle deadlines en terugkomdagen in mijn agenda genoteerd en vervolgens heb ik een opzet gemaakt voor mijn stageplan en zo ver mogelijk uitgewerkt. Mijn leerdoelen en stageplan wil ik dan ook morgen met Rick doornemen. 
Ik heb de middag afgesloten door aan mijn blog te werken.

8 uur gewerkt vandaag.
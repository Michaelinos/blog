---
title: Flyer Hockey
date: 2019-09-23
---

De ochtend begonnen met keek op de week. Hier de planning van deze week doorgenomen. Ik heb vandaag voornamelijk voor mezelf gewerkt. Er was niet zo heel veel te doen dus ik heb al mijn blog artikelen bijgewerkt waar ik mee achterliep en al mijn bestanden netjes opgeruimd en gesorteerd.

In de middag kwam Alex naar mij toe met de vraag of ik een flyer wil maken voor de hockey vereniging in Pijnacker. Hier heb ik copy en foto’s voor aangeleverd gekregen. Ik heb verschillende iteraties gemaakt en deze aan Alex laten zien. Uiteindelijk had ik aan het eind van de middag een flyer aangeleverd waar Alex tevreden mee was.

8 uur gewerkt vandaag.

---
title: RET Design Challenge + Presentatie + bezoek stagebegeleider
date: 2019-10-17
---

Vandaag stond in het thema van de RET design challenge. De RET weet zijn zakelijke doelgroep niet goed genoeg te bereiken met de huidige pagina voor deMobiliteitsmanager (soort NS-Businesscard). Aan ons de taak om een alternatieve flow te verzinnen die bezoekers online kunnen doorlopen zodat de conversie verhoogd wordt. Dit werken wij uit in een nieuwe landingspagina voor de zakelijke RET klant.

Om te beginnen was mijn taak om goede foto’s te vinden voor het openingsbeeld van de pagina. Na eindeloos zoeken had ik foto gevonden die potentieel gebruikt kan worden voor de lander. Een foto van een man met een visitekaartje, wat gefotoshopt kan worden naar de RET kaart.

Vervolgens heb ik allerlei logo’s en iconen verzameld van vervoersbedrijven die gebruikt kunnen worden met de RET kaart. Deze heb ik allemaal gecategoriseerd en gedeeld met de rest van de ontwerpers.

Na de lunch was het alweer tijd voor mijn PechaKucha presentatie. Deze is eigenlijk heel soepel verlopen waar ik erg blij mee ben. Tevens via Slack aan mijn collega’s gevraagd of zij mijn feedbackformuliertje hiervoor willen invullen.

Na mijn presentatie kwam mijn stagebegeleider van school langs voor een gesprek. Dit is ook erg goed gegaan, genoeg verbeterpunten waar ik mee aan de slag kan gaan.
Hierna weer verder gegaan met de RET visualisatie. Ik heb een RET kaartje in de foto gefotoshopt. En de logo’s van medewerkende vervoersbedrijven in een cirkel aan de foto toegevoegd. Tevens ook een Rotterdamse achtergrond in de foto gezet.

8 uur gewerkt vandaag.

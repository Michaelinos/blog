---
title: BE project verdere uitwerking + Mohsen feedback
date: 2019-10-24
---

Vandaag in de ochtend verder gegaan met de drie ontwerpen voor het BE project. Ik heb alle drie de ontwerpen op een groffe manier visueel uitgewerkt om een beter beeld te krijgen van het eindresultaat.

Toen ik hier klaar mee was, heb ik aan Rick feedback gevraagd op mijn Spotify voor CarPlay. Na een korte uitleg van mij, gaf hij als feedback:
-	Maak grotere panelen voor de album cover, zodat je er doorheen kan swipen
-	Je wilt in de auto zo min mogelijk op knopjes drukken, probeer iets met gestures uit te werken voor de navigatie door nummers, bij het liken en als je een nummer wilt repeaten.
-	Voeg ook een ‘vorig nummer’ knop toe, dit is gebruikelijk in muziek navigatie
-	De ‘back’ knop kan je boven in de navigatie verwerken, scheelt weer ruimte.

Vervolgens ben ik aan de slag gegaan om deze feedback te verwerken in een nieuwe versie van mijn ontwerp.

Om 14.00u was het alweer tijd om met Mohsen een overleg te hebben over de ontwerpen van het BE project. Hij gaf mij als feedback:
-	De losee contact knop + telnr/email voelt dubbel op. Verwerk telnr en email in twee knoppen.
-	Voeg onder ‘diensten’ sectie testimonials toe. Het is misschien cheesy maar het werkt heel goed.
-	Voeg bij de recente projecten logo’s van de klanten toe en een beschrijving met titels.
-	Je kan ook beter een contactformulier op het einde van de pagina toe voegen i.p.v. alleen een link naar de contactpagina
-	Zoek betere afbeeldingen, meer kleur en test verschillende fonts

Vervolgens de rest van de dag bezig geweest om de gekozen ontwerpen verder visueel uit te werken.

8 uur gewerkt vandaag.

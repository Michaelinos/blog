---
title: Animatie ABR
date: 2019-09-25
---

Heel de dag gewerkt aan de animatie voor de aanmeldbevestiging van ABR. Ik heb verschillende voorbeelden opgezocht, filmpjes gekeken over After Effects en ben vervolgens zelf aan de slag gegaan. Vervolgens had ik een eerste versie gemaakt met zwevende bubbels in een loop.

Deze heb ik aan Rick laten zien, maar dit was toch niet helemaal de bedoeling.
De animatie moet eerder een soort explosie zijn waarbij de bellen uit de koker schieten en vervolgens knappen. Dit animeren is nogal een tijdrovend project, ik ben hier dan ook de rest van de dag mee bezig geweest.

8 uur gewerkt vandaag.

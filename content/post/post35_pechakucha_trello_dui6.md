---
title: PechaKucha research + feedback Trello + DUI6
date: 2019-10-07
---

De ochtend ben ik begonnen met een eerste onderzoek naar PechaKucha. Hierdoor kwam ik bij deze link: https://www.forbes.com/sites/markmurphy/2015/10/18/this-unusual-japanese-technique-will-radically-improve-your-presentations/#213aa81d1819.

Hierdoor werd het voor mij duidelijker wat het nou precies inhoudt en wat het doel is van zo’n presentatie. Kort hierna kwam Rick naar mij toe om feedback te geven op mijn Trello bord die ik afgelopen vrijdag had gemaakt:

-	Ik had veel user stories geschreven uit het perspectief van de ontwerper, wat niet handig is. Zo kan je niet bepalen welke zaken prioriteiten hebben voor de gebruiker.
-	Momenteel staat alles per sprint geplant, wat niet de bedoeling is van Scrum. Maak vijf borden: To Do (backlog), Huidige Sprint, In Progress, Review en Done. Gooi alles in de backlog en pak er allleen dingen uit die je deze sprint gaat doen. Zo behoud je het overzicht.
-	Maak grotere epic’s die je door de verschillende sprints mee kan nemen met subtaken die ja kan afvinken.
-	Voeg acceptance criteria toe aan je user stories.

Kort hierna kwam Gitva naar mij toe met de vraag om nog eens te kijken naar de awardkalender. Ze wilt ook de datums van uitreikingen die al eerder dit jaar zijn geweest zodat hier voor volgend jaar rekening mee gehouden kan worden. Hier ben ik de rest van de ochtend mee bezig geweest.

In de middag heb ik de nieuwsbrief voor RAAK verder uitgewerkt en heb ik gewerkt aan een Daily UI Challenge voor een social media profiel.

8 uur gewerkt vandaag.

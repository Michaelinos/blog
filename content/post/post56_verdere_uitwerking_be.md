---
title: Verdere uitwerking BE
date: 2019-11-07
---

Gisteren heb ik aan mijn broer (opdrachtgever BE) de nieuwste versie van het ontwerp gepresenteerd. Hierbij heb ik erop gelet dat ik onderbouwing geef waarom ik bepaalde keuzes heb gemaakt in mijn ontwerp. Ik heb een goed gesprek over het ontwerp. Er zijn een aantal tekstuele wijzigen gebeurt. De opdrachtgever is tevreden over de diensten, projecten en testimonial secties. Wat veranderd moet worden:
-	Ander openingsbeeld
-	Kabels rustiger maken
-	Huisstijl BE meer terug laten komen
-	Contactformulier slopen. Groot vlak voor telefoonnummer, email en google maps erin verwerken.

Vandaag ben ik bezig geweest om om deze feedback te verwerken. Hierbij heb ik een aantal keer feedback gevraagd bij Mohsen, met kleine wijzigingen tot gevolg. Tevens heb ik meerdere versies gemaakt van het openingsbeeld zodat ik wat heb om te laten zien.

Ook ben ik vandaag begonnen met het maken van de projectenpagina. Hier heb ik een eerste opzet voor gemaakt en aan Mohsen laten zien. Hij en ik waren het erover eens dat deze eerste versie nogal saai is. Daarom heb ik online opnieuw gezocht naar inspiratie. De rest van de middag ben ik bezig geweest met het maken van een nieuwe versie van de projectenpagina.

8 uur gewerkt vandaag.

---
title: Brainstorm Bomenbanen
date: 2019-12-02
---

Vandaag ben ik mee geweest naar een brainstormsessie bij Bomenbanen. Dit was met Martin Kuipers (PM) en Rick van der Wal, mijn stagebegeleider. Bomenbanen wilt een nieuwe merkidentiteit voor de opleiding die ze faciliteren op het gebied van boombeheer.
Hun doel is om voor deze opleiding 20 nieuwe studenten aan te trekken. De focus wordt gelegd op scholieren die net HAVO hebben afgerond. Deze brainstorm hebben wij gedaan in samenwerking met drie werknemers van Bomenbanen. De opzet van de dag in het kort:
-	Bijpraten over de uitgangspunten van de identiteit
-	Een duidelijke productdefinitie
-	Longlist met kernwoorden en ideeën voor de merknaam
-	Duidelijk beeld van de vervolgstappen/het proces

Na het bijpraten over de uitgangspunten van de identiteit hebben wij met z’n alle een korte brainstorm gedaan in de vorm van een longlist. Vervolgens hebben we de een richting voor de identiteit vastgesteld doormiddel van het omcirkelen van termen die volgens Bomenbanen passen bij de opleiding. Eenzelfde soort proces hebben we gedaan voor de merknaam.

Rond 12u ’s middags waren wij klaar met de brainstorm. Over het algemeen ging deze brainstorm best wel stroef. De aanwezige werknemers ervaarden de dag als ongemakkelijk en participeerde daardoor moeizaam. Er was ook niet zo’n goede groepsdynamiek. Ikzelf vond het een leerzame en interessante ochtend waardoor ik heb kunnen ervaren hoe RAAK zo’n dag aanpakt.

Rond 15u waren wij weer op kantoor waardoor ik niet zo veel meer kon doen. De rest van de middag heb ik mijn weekplanning afgemaakt en specifiek per dag gekeken wat ik wil gaan bereiken.

8 uur gewerkt vandaag.

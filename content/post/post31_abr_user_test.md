---
title: ABR user test
date: 2019-10-01
---

Twee weken terug ben ik druk bezig geweest met de lander voor ABR. Uiteindelijk is hier een ontwerp uit gekomen dat door de developers is gerealiseerd in een testomgeving (website). Rick heeft mij gevraagd om deze website uitvoering te testen doormiddel van een user test. Mijn bevindingen heb ik opgeschreven door te zeggen: ik verwachtte dit… en er gebeurde dit… Op deze manier is het voor de lezer duidelijker wat ik bedoel.

Ik heb hierbij verschillende browsers en verschillende weergaven geprobeerd, zowel mobiel als desktop. En zoveel mogelijk opties aanzetten of breken om te kijken wat allemaal werkt. Hier ben ik eigenlijk heel de dag mee bezig geweest en dit allemaal in een apart verslag uitgewerkt. Dit verslag heb ik vervolgens naar Rick en Mike gestuurd. De rest van de dag heb ik gewerkt aan wat dingen voor school.

8 uur gewerkt vandaag.

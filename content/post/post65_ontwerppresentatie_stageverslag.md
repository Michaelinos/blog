---
title: Voorbereiding ontwerppresentatie + stageverslag
date: 2019-11-20
---

A.s. donderdag ga ik een ontwerppresentatie organiseren om te laten zien wat ik heb gemaakt voor het Betlej Elektrotechniek project. Hiervoor heb ik de volgende structuur bedacht:
-	Context over Betlej Elektrotechniek
-	Doel van herontwerp
-	Kernwaarden Betlej Elektrotechniek
-	Desktopversie ontwerp
-	Mobiele versie ontwerp

Vervolgens heb ik screenshots gemaakt van mijn ontwerpen en de presentatie in PowerPoint gemaakt volgens bovenstaande structuur. Ik heb mijn presentatie aan Mohsen laten zien en hij gaf mij de volgende feedback:
-	Vormgeving kan beter, het is momenteel nogal saai. Dit geeft de toeschouwer de indruk dat je niet veel tijd hebt gestoken in de presentatie en dus ook de ontwerpen. Voeg wat elementen toe uit de Betlej huisstijl en plaats bijvoorbeeld een logo van het bedrijf toe in de dia’s
-	Plaats de desktop screenshots in een tabletformaat of webbrowser, dit geeft context aan je ontwerpen
-	Laat de oude versie en nieuwe versies zien en geef aan wat je minder goed vindt en hoe je dit hebt verbeterd, zo kan je je ontwerpkeuzes onderbouwen

Daarom ben ik de rest van de ochtend bezig geweest om deze feedback te verwerken in mijn presentatie.

In de middag heb ik weer gewerkt aan mijn stageverslag:
-	STARRT over presenteren opnieuw gemaakt volgens feedback van Rick
-	Tweede STARRT gemaakt over verdieping UX

8 uur gewerkt vandaag.

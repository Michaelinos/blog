---
title: Ontwerppresentatie + stageverslag
date: 2019-11-21
---

Voornamelijk bezig geweest met mijn stageverslag. Zo heb ik nog twee STARRT’s uitgewerkt en een stukje over RAAK als bedrijf. Nu heb ik alle inhoudelijke teksten voor mijn verslag uitgeschreven.

In de middag was het tijd voor mijn ontwerppresentatie. In deze situatie was Rick zogenaamd de opdrachtgever, Fabian en Mohsen zaten erbij om feedback te geven over mijn ontwerpen en vorm van de presentatie. Hierbij heb ik de volgende feedback ontvangen:
-	Je ging iets te snel door de presentatie heen, je hoeft niet constant te praten. Geef mensen de tijd om te kijken naar wat je hebt gemaakt.
-	Door de losse schermen raak je de overzicht kwijt over waar je bent op de website, geef mensen context.
-	Altijd het volledige scherm gebruiken wanneer je ontwerpen live laat zien. Dat maakt de presentatie professioneler en geeft rust op het scherm.
-	Je mag trotsheid laten zien tijdens je presentatie. Dat geeft de klant het gevoel dat je geeft om wat je hebt gemaakt voor hun.
-	En nog inhoudelijke feedback op de ontwerpen

Hierna een afscheidsborrel gehad van een collega die het bedrijf verlaat.

8 uur gewerkt vandaag.

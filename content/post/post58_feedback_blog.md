---
title: Feedback onderzoek HCD + blog bijwerken
date: 2019-11-11
---

In de ochtend een stuk over inzichten en een conclusie geschreven in mijn HCD-onderzoek. Vervolgens het document naar Rick gestuurd voor feedback. De rest van de dag bezig geweest om mijn blog goed bij te werken, hier liep ik een beetje achter. Aan het eind van de middag heb ik feedback op mijn verslag gekregen van Rick:

-	Inleiding gebruiken als korte uitleg over inhoud
-	Schuttingtaal eruit
-	Niet vanuit ik vorm; We gebruiken
-	Nummernotatie gebruiken voor fases
-	Voorbeelden technieken aanhalen in fases
-	Bij ideation = lijstje met goede en slechte voorbeelden
-	Beter voorbeeld praktijk (geen Norman door)
-	Ketchupfles uitgebreider met verschillende prototypes
-	Conclusie en inzichten mogen wat abstracter beschreven worden. Je schrijft dit verslag zodat de lezer er wat aan heeft. Het leest nu als een schoolverslag
-	Maak een bronverwijzing bij de hoofdstukken

8 uur gewerkt vandaag.

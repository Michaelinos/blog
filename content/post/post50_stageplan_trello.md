---
title: Stageplan + feedback Trello
date: 2019-10-30
---

Vandaag ben ik heel de dag bezig geweest met mijn stageplan. Voor a.s. maandag is het de bedoeling dat ik een tweede versie inlever met vervolgstappen hoe ik mijn leerdoelen ga aantonen. Hier ben ik heel de dag mee bezig geweest. Aangezien Rick vandaag niet aanwezig was, heb ik vragen over tijdsplanning en onduidelijkheid over het aantonen van dingen opschreven om morgen te vragen.

Tevens heb ik samen met Fabian mijn trello doorgenomen. Hij heeft mij praktische tips gegeven over hoe ik Trello het beste kan inrichten. Bijvoorbeeld:
-	Einddatums koppelen aan sprints
-	User stories in concrete stapjes beschrijven
-	Zo min mogelijk ruis in de planning met dubbele taken
-	Taken doorschijven naar volgende sprint als je ze niet afkrijgt

Deze feedback heb ik verwerkt waardoor ik een veel duidelijkere planning heb gekregen.

8 uur gewerkt vandaag.

---
title: Feedback PvA BE + DUI5
date: 2019-09-24
---

Dag begonnen met een korte standup meeting over waar ik mee bezig ben en wat ik ga doen.

Van Alex de opdracht gekregen om een email banner te maken voor de mail die naar leads worden gestuurd i.v.m. nieuw RAAK lander. Ook wat typfoutjes verbeterd in de flyer voor Alex.

In de middag heeft Rick mij feedback gegeven op het plan van aanpak dat ik heb gemaakt voor Betlej Elektrotechniek:
-	Alle technische eisen mogen ervoor nu uit. Anders gaat je opdracht te groot worden. Kan later alsnog gemaakt worden.
-	Beschrijven wie aan wat gaat werken: hoe ziet het Scrum team eruit.
-	Datums en tijden koppelen aan de sprint.
-	Duidelijk aangeven hoelang een sprint duurt en wanneer je eraan gaat werken.
-	Opdracht duidelijker omschrijven

Met deze feedback ben ik aan de slag gegaan en doorgevoerd in een tweede versie. Rick heeft het goedgekeurd en zei dat ik niet moet vergeten deze dagen te blocken in mijn agenda en voor de sprints een kanban bord maken. Tevens heb ik ook voor woensdagavond met mijn broer, de opdrachtgever, afgesproken om duidelijk af te spreken wat hij wil.

Rest van de dag voor mezelf gewerkt aan een daily UI challenges waarbij ik een app icoon moest ontwerpen.

8 uur gewerkt vandaag.

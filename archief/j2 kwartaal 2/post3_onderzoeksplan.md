---
title: Onderzoeksplan
date: 2018-11-23
---

In de ochtend ben ik met mijn team bezig geweest om een onderzoeksplan te maken voor deze sprint. We hebben momenteel als doelgroep sociale huurders van middelbare leeftijd gekozen, maar na dit besluit hebben we geen onderzoek meer verricht naar de doelgroep.

Als eerste heb ik in kaart gebracht wat we nu wilden weten van de doelgroep voor ons onderzoek. Hier is o.a. uitgekomen dat we de kenmerken van de doelgroep willen weten, welke communities momenteel actief zijn in Rotterdam en wat voor platform wij willen gebruiken voor ons concept.

Ik heb samen met Jari de taak op me genomen om onderzoek te doen naar de kenmerken van de doelgroep en wat voor communities momenteel actief zijn in Rotterdam. Ook gaat ieder voor zich vier fieldresearch technieken verzamelen die wij deze week willen gebruiken om de doelgroep beter te leren kennen. De rest van de dag ben ik bezig geweest met deskresearch over de doelgroep/communities.

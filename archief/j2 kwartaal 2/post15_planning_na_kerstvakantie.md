---
title: Planning na kerstvakantie
date: 2019-01-08
---

Het was weer even geleden dat we aan het project hebben gewerkt i.v.m. de kerstvakantie. Vandaag hebben we weer een vergadering gehouden om te kijken waar we nu zijn gebleven na de vakantie. Wat we nu hebben afgesproken is dat Jari voor het mobiele prototype zorgt en ik voor de desktop versie. Josephin maakt voor ons allebei wireframes in Adobe XD die Jari en ik verder uitwerken.

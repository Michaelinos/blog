---
title: Feedback ontwerpverslag + prototype
date: 2019-01-12
---

Afgelopen vrijdag hebben we van Bob feedback gekregen op ons ontwerpverslag. Ik heb deze feedback doorgenomen en gedeeld met de rest van het team. Ook heb ik een document gemaakt met alle feedback en wat er nu concreet aangepast moet worden. Dit heb ik ook weer verdeeld zodat iedereen zijn eigen gemaakte werk verbeterd:

-	Wijkonderzoek (Lorenzo)
-	Verslag creatieve technieken (Andor)
-	Conceptkeuze onderbouwing (Michael)
-	Ontwerpcriteria (Josephin)
-	Conceptbeschrijving (Michael)
-	Prototype leefstad (Jari)
-	Testplan (Josephin)
-	Testrapportage (Josephin)

De rest van de dag ben ik bezig geweest met het ontwerpen van het desktop prototype.

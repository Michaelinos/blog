---
title: Conceptbeschrijving verbeterd
date: 2019-01-15
---

In de vergadering heb ik besproken waar iedereen nu mee aan de slag kan om de feedback van Bob op het ontwerpverslag toe te passen. Ook heb ik mijn voortgang met het prototype aan de rest van het team laten zien.

Hierna ben ik bezig geweest met de feedback over de conceptbeschrijving te verwerken in een nieuwe versie. I.p.v. een opsomming heb ik er nu een verhaal van gemaakt waarbij een duidelijke lijn van abstract naar concreet is beschreven over het concept.

---
title: Design rationale, conceptbeschrijving + onderbouwing
date: 2019-01-16
---

Vandaag heb ik de conceptbeschrijving volledig aangepast. Ook heb ik de conceptonderbouwing verbeterd waarbij ik de waarden en behoeften van onze doelgroep heb gekoppeld aan het concept. Dit is nu zowel op feature niveau als ervarings niveau beschreven.

Ten slotte heb ik een design rationale gemaakt waarbij ik aan de hand van ons onderzoek heb beschreven hoe elk onderdeel van het concept inspeelt op de waardes en behoeftes van onze doelgroep.

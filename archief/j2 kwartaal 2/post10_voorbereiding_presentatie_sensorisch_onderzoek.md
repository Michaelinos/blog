---
title: Voorbereiding presentatie + sensorisch onderzoek
date: 2018-12-06
---

Morgen hebben we een presentatie voor Woonstad waarbij we onze voortgang met ons project presenteren. Daarom zijn we vandaag aan de slag gegaan met het voorbereiden van onze presentatie. We hebben een indeling gemaakt wie wat gaan presenteren en wat we willen laten zien. Ook heb ik de taak op mij genomen om de powerpoint presentatie te maken. Hierbij heb ik bij alle dia’s steekwoorden gebruikt en af en toe illustraties toegevoegd.

In de middag hebben we ook een sensorisch onderzoek gedaan. We hebben de verschillende kernwaarden van onze doelgroep/het concept opgeschreven en vervolgens heeft ieder voor zich beschreven wat hij of zij hierbij voelt, ziet, ruikt, etc. Dit hebben we vervolgens met elkaar besproken. Het was wel grappig om te zien dat iedereen vaak totaal verschillende dingen had opgeschreven. Deze inzichten kunnen we vervolgens gebruiken voor het ontwerp van ons concept en om onze stand aan te kleden op de eindexpositie.

De rest van de dag ben ik bezig geweest met het uitwerken van mijn digitale portfolio over mezelf.

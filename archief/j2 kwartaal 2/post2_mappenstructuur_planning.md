---
title: Mappenstructuur + planning
date: 2018-11-22
---

In de middag zijn we even bij elkaar gekomen op school. Ik heb een vergadering gehad waarbij we hebben besloten wat de standaardnaamgeving wordt voor onze dropbox bestanden. Vorige sprint was onze dropbox nogal chaotisch geworden dus gaan we dit nu anders aanpakken. De naamgeving wordt nu bestandsnaam_naam_versie. Ook hebben we onze planning voor morgen gemaakt. Morgen ga ik aan de slag met het onderzoeksplan voor onze doelgroep.

---
title: Debriefing, nieuwe rollen + planning
date: 2018-11-20
---

Vandaag was de eerste dag van het nieuwe kwartaal. Ik heb een debriefing gekregen met wat er deze sprint van mij en mijn groepje verwacht wordt. Deze heeft ieder voor zich doorgenomen. Na de briefing hebben we onze eerste vergadering gehouden volgens een vaste indeling. Dit heb ik vorige sprint samen met het team besloten zodat we wat meer consistentie en houvast hebben tijdens het kwartaal.

Bij deze vergadering hebben we een aantal taken verdeeld die wij voor a.s. donderdag willen volbrengen. Ik heb samen met Lorenzo de taak op me genomen om de planning van deze sprint in Trello te maken. Ook hebben we de rollen anders verdeeld omdat we ons op andere vlakken willen verbeteren. Hierbij heb ik gekozen om deze sprint de visual designers labs te gaan volgen omdat ik mijzelf hierop wil verbeteren. Verder hebben we besloten vanmiddag de golden circle techniek gaan toepassen, onze labs volgens een sjabloon gaan documenteren en een betere mappenstructuur willen op Dropbox omdat dit vorige sprint nogal een zooitje was geworden.

Na de vergadering ben ik met Lorenzo aan de slag gegaan om de planning te maken. We zijn begonnen met alle deliverables en deadlines dit kwartaal in kaart te brengen op papier. Deze werk ik morgen verder uit in Trello zodat wij dit donderdag met elkaar kunnen bespreken.

In de middag heb ik m’n eerste visual design lab gehad. Hier heb ik uitleg gekregen over vormstudie, hoe je digitale producten kan analyseren en hoe je snel kan schetsen. Na de uitleg zijn we aan de slag gegaan met het maken van een iconset voor ons concept.

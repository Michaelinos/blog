---
title: Update indeling ontwerpverslag
date: 2018-12-09
---

Vandaag ben ik eigenlijk heel de dag bezig geweest met de indeling van ons ontwerpverslag bij te werken naar de richtlijnen in onze visual styleguide. Ook heb ik een sjabloon gemaakt die gebruikt kan worden door de rest van het team om zijn of haar stukken in dezelfde stijl te maken. Tenslotte heb ik rest van het team geïnformeerd over de lopende deadlines en waar zij het sjabloon kunnen vinden.

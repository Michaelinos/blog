---
title: Bespreking ontwerpverslag
date: 2018-12-11
---

Vandaag heb ik met de rest van het team besproken hoe we het ontwerpverslag gaan inrichten volgens het sjabloon dat ik afgelopen zondag heb gemaakt. Hierbij zet iedereen zijn eigen gemaakte documenten om naar een inDesign bestand dat ik vervolgens kan importeren in het ontwerpverslag.

De rest van de dag ben ik bezig geweest met zo veel mogelijk voortgang te maken met het ontwerpverslag.

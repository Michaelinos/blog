---
title: Filmpje Leefstad
date: 2018-11-29
---

Andor had de filmstudio op school voor ons geserveerd dus konden we deze gebruiken voor het uitwerken van een scenario door het maken een filmpje. Bij dit scenario wordt uitgebeeld hoe iemand een kapotte tv heeft, en deze door het gebruik van Leefstad gemaakt kan krijgen. Van tevoren had Andor hier een script voor uitgewerkt. Rond 14.00u waren we klaar met filmen en zijn Andor en Lorenzo verder gegaan met het editen van het filmpje.

Ik ben samen met Jari aan de slag gegaan met het maken van een conceptposter. Ik ben begonnen met het maken van 4-5 verschillende indelingen op papier. Uiteindelijk ben ik met mooiste indeling verder gegaan. Dit heb ik verder uitgewerkt in Illustrator.

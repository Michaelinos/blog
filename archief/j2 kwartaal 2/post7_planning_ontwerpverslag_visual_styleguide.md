---
title: Planning ontwerpverslag + visual styleguide
date: 2018-12-03
---

Vandaag zijn we de dag begonnen met een teamvergadering. Hierbij hebben we een planning gemaakt voor het volgende ontwerpverslag. We hebben taken verdeeld wie wat gaat bijwerken en wat er nog toegevoegd moet worden. Ik ga aan de slag met de visual styleguide, concept onderbouwing en beschrijving.

De rest van de dag ben ik bezig geweest met het maken van een styleguide in Illustrator die wij kunnen hanteren voor onze documenten en ons prototype.

---
title: Feedback prototype & testplan
date: 2018-03-27
---

In de ochtend heb ik samen met Hanno en Andor de uitgewerkte prototype schermen van gisteren uitgesneden en op karton geplakt met bijschriften. Nadat we hier mee klaar waren hebben we samen voor het testplan interview vragen samengesteld, vervolgens heb ik het document verder uitgewerkt.

In de middag hadden we een feedback moment gehad voor ons testplan. We hadden een erg goed gesprek met de docent, ze heeft ons goede feedback gegeven op zowel het testplan en ons prototype. Deze feedback was heel erg nuttig en gaan we zeker gebruiken het volgende kwartaal.

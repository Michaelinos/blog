---
title: Styleguide & Lifestyle Diary
date: 2018-03-12
---

We hebben allereerst gewerkt aan een styleguide voor onze documenten. Hierin hebben we bepaald wat voor fonts en kleuren we gaan gebruiken voor onze documentatie. Vervolgens hebben we met elkaar besproken hoe het allemaal ging met het bijhouden van foto’s voor de lifestyle diary. Sinds deze opdracht individueel was, heb ik in het klad uitgewerkt hoe ik de lifestyle diary ongeveer wil maken.

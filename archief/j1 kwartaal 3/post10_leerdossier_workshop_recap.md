---
title: Leerdossier, workshop & recap
date: 2018-03-28
---

We hebben in de ochtend samen met ons groepje geïnventariseerd wat we nog allemaal moeten doen voor het project en ons leerdossier. Andor en Hanno hebben de ontwerpcriteria verbetert. Indra en ik hebben de recap gemaakt waarin we alle gemaakte deliverables hebben samengevat en uitgewerkt tot een poster. Hierna hebben we verder gewerkt aan ons leerdossier

In de middag hebben we een workshop rapid prototyping gevolgt. Hierin hebben we een live mobiel prototype gemaakt en dit gestyled met html & css. Na de workshop hebben we heel de avond verder gewerkt aan het leerdossier.

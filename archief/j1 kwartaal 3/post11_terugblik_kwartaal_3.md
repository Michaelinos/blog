---
title: Terugblik kwartaal 3
date: 2018-03-29
---

Er was een trage start dit kwartaal, maar de laatste paar weken hebben we ontzettend veel bereikt als groep. Ook verliep de samenwerking heel soepel, we konden als team goed samenwerken en het was erg gezellig.

Doordat ik vorig kwartaal veel deliverables heb gemaakt, heb ik die opgebouwde kennis goed kunnen gebruiken bij dit project waardoor we sneller goed uitgewerkte documenten hadden.

---
title: Plan van Aanpak
date: 2018-02-14
---

Vandaag was de deadline voor het plan van aanpak en er moest hier ook een poster van gemaakt worden. Het plan van aanpak hebben we eerder van de week afgemaakt maar de poster moest nog gemaakt worden. We zouden dit de dag van te voren maken maar omdat de helft van het team niet aanwezig kon zijn, hebben we besloten het vandaag te maken.

We zijn samen gaan zitten en door de taken te verdelen hebben we alles op tijd af kunnen krijgen. Vervolgens moesten we ons plan van aanpak pitchen aan Jantien, zij heeft deze vervolgens gevalideerd en feedback aan ons gegeven. De feedback die we hebben gekregen was dat we onze visie moesten verwerken in het PvA en de verbeterde versie vervolgens aan haar moeten laten zien.

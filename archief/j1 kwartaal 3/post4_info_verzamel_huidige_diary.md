---
title: Informatieverzameling huidige lifestyle diary
date: 2018-03-07
---

Vandaag hebben we een begin gemaakt aan de huidige lifestyle diary. Allereerst hebben we het BRAVO-model er bij gepakt en bepaald wat voor informatie we per categorie willen weten. Dit hebben we uitgewerkt op een groot papier die hieronder zichtbaar is.

We hebben uiteindelijk afgesproken dat ieder project lid twee dagen lang alles bijhoud dat hij die dag doet waarvan één doordeweekse dag en één dag in het weekend. Ook moet er van elke maaltijd een foto gemaakt worden.

---
title: Reflectie kwartaal 2 & Scrumboard
date: 2018-02-21
---

In de ochtend hebben we SLB gehad, in deze les moesten we reflecteren op onze competenties. Ik heb per competentie moeten kijken hoe ver ik deze heb aangetoond en op die manier moeten bepalen aan welke competenties ik nog moet werken. Ook heb ik moeten reflecteren op de feedback die ik heb gekregen op mijn leerdossier zodat ik deze feedback kan meenemen naar OP3. De resultaten zijn zichtbaar op onderstaande foto’s.

In de middag hebben we een fysiek scrumboard gemaakt waar we dag taken op gaan zetten. De taken worden verdeeld door te overleggen met elkaar wie wat wil maken en met wat iemand moeite heeft zodat die persoon zichzelf kan verbeteren. Op deze manier leert iedereen alles maken. We hebben vandaag het samenwerkingscontract en de PvA poster afgemaakt waar we nog feedback op moeten vragen. Er wordt nu nog gewerkt aan de lifestyle diary, huisstijl, debriefing en onderzoekersmethode welke we af moeten hebben na de voorjaarsvakantie. Ook hebben we onze naam veranderd naar The Brave Boobies.

![Feedback Kwartaal 2-1](blog/content/photos/feedback_kw2_1)
![Feedback Kwartaal 2-2](blog/content/photos/feedback_kw2_2)

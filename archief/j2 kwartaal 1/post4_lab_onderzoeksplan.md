---
title: Lab & Onderzoeksplan
date: 2018-09-14
---

In de ochtend ben ik naar het interaction design lab geweest. Hier heb ik uitleg gekregen over wat een user scenario inhoudt en hoe het wordt gebruikt. Vervolgens heb ik twee scenario’s uitgewerkt over tikkie. Eén scenario gaat over een oude man die in een café voor zijn vrienden een betaalverzoek maakt en één scenario over een moeder die de oppas moet betalen via een betaalverzoek.

In de middag is besproken dat wij ons vanaf nu kunnen inschrijven voor een bezoek bij de huismeester of balie. Hierbij heb ik gekozen een ochtend met de huismeester mee te lopen. Dit wordt 20 september van 9u tot 10.30u. Nu ik de datum weet, ben ik begonnen met het maken van een onderzoeksplan. Allereerst heb ik aan de hand van hoofd en sub onderzoeksvragen een aantal onderzoeksmethoden vastgesteld. Hierin heb ik besloten dat ik wil observeren en een semigestructureerd interview wil gaan doen. Vervolgens heb ik ook interviewvragen opgesteld, voor zowel bewoners als vragen aan de huismeester. Om snel gegevens over het bezoek van bewoners te noteren, ben ik ook de slag gegaan met een scoresheet. Hier staan gegevens op zoals bijv. lengte bezoek, man/vrouw, het probleem, reactie van de bewoner, etc.

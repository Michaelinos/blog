---
title: Deskresearch
date: 2018-09-18
---

Vandaag heb ik een begin gemaakt aan deskresearch over de doelgroep sociale huurders. Hierbij heb ik gezocht naar kenmerken van de doelgroep, waar sociale huurwoningen precies staan in Rotterdam en verschillende nieuwsartikelen. Ook heb ik feedback gevraagd op mijn onderzoeksrapport. Hier kreeg ik als feedback dat ik iets te veel onderzoeksvragen had, ik moet terug gaan kijken naar de debriefing en wat meer focussen op hoe ik het klantcontact kan verbeteren. Dit heb ik vervolgens aangepast en een aantal nieuwe interviewvragen opgesteld.

Vervolgens heb ik met een klasgenoot laddering geoefend, dit is een onderzoeksmethoden waarbij je met een aantal basisvragen zo lang mogelijk doorvraagt naar het waarom, om zo tot de kernwaarde van die personen te komen.

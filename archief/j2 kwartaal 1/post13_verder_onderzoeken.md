---
title: Verder Onderzoeken
date: 2018-10-02
---

Na de uitleg van Bob over de planning van vandaag hebben we als groepje met elkaar besproken wat goed gaat in de samenwerking en wat er verbeterd kan worden. Hier kwam o.a. uit dat het bespreken van voortgang, aanwezigheid en het behalen van deadlines goed gaat. Verbeterpunten zijn o.a. mening delen, blog bijhouden en de opgedane kennis bespreken. Hier willen we dan ook actief aan gaan werken om dit te verbeteren.

Hierna hebben we de projectvoortgang met elkaar besproken. Ik zei dat ik van mening ben dat wij iets te weinig onderzoek hebben verricht om een goed concept te verzinnen. Hier was de rest van het team mee eens, dus zijn we aan de slag gegaan. Jari en Andor zijn aan de slag gegaan met het verzinnen van een goede Facebook post die wij kunnen delen om zo mensen uit onze doelgroep te kunnen spreken. Verder heb ik gekeken naar onderzoeksmethoden die wij kunnen inzetten, hierbij heb ik gekozen voor een bagtour. Dit is een onderzoeksmethode waarbij je willekeurige mensen op straat vraagt naar de inhoud van hun tas om er zo achter te kunnen komen wat voor hun belangrijk is in het dagelijkse leven. Ook heb ik contact opgenomen met een kennis voor nog een laddering interview.

In de middag heb ik aangesloten bij het lab van interaction design omdat ik dit lab deze sprint wil gaan volgen. Hier heb ik uitleg gekregen over wat er vorige sprint allemaal behandeld is in het lab. Vervolgens ben ik verder gegaan met het doornemen van alle presentaties en daarna ben ik zelf aan de slag gegaan met wat dingen voor mezelf.

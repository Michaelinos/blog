---
title: Ontwerpverslag Afwerking
date: 2018-10-04
---

Het ontwerpverslag heb ik vandaag voor 95% uitgewerkt. Ik heb alle bevindingen in de juist hoofdstukken opgedeeld, overal conclusies toegevoegd en de bronvermeldingen volgens de APA-regels in het document gezet. Ook heb ik gecontroleerd dat alle deliverables erin zaten en heb ik het document consistent vormgegeven. Er ontbreken nog alleen twee documenten die ik morgen van Josephin en Lorenzo opgeleverd krijg. 

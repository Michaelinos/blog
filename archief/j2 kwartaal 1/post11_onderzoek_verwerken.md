---
title: Onderzoek Verwerken
date: 2018-09-27
---

Vandaag zijn we in de middag bij elkaar gekomen als groep. Iedereen is druk bezig geweest met zijn of haar deliverables af te maken. Ik heb heel de dag gewerkt aan het onderzoeksplan/ontwerpverslag. Hierbij heb ik al het onderzoek verloop en de resultaten verwerkt in een document. Zo nodig heb ik inleidingen en conclusies geschreven. Bij deliverables die ik zelf niet heb gemaakt heb ik gevraagd of die persoon een conclusie van zijn bevindingen wilt opschrijven. Ook heb ik ervoor gezorgd dat het document er verzorgt uitziet en dat alles consistent is vormgegeven.

Ook heb ik Jari feedback gegeven over de vormgeving van de deliverables en heb ik geholpen met het maken van de poster, waar Jari mee bezig was.

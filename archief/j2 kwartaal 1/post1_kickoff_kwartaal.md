---
title: Kickoff Leerjaar 2
date: 2018-09-04
---

Vandaag was weer de eerste dag van het schooljaar. In de ochtend was gelijk de kick-off van ons nieuwe project. De opdrachtgever hiervan is Woonstad. Ik zat ver achteraan tijdens de presentatie, en het geluid was niet goed geregeld. Hierdoor heb ik helaas niet veel kunnen verstaan over de inhoud van het project. Wel heb ik gehoord dat de doelgroep van mijn klas te maken heeft met sociale huurwoningen.

Na de kick-off zijn we naar onze klassen gegaan. Dit jaar zit ik in CMD2A. Ik heb afgelopen week aangegeven dat ik eventueel teamleider wil worden en dit kwartaal ben ik daar dan ook voor gekozen. De teamleiders zijn apart genomen en kregen een stapel blaadjes waar alle klasgenoten stonden. Op basis van karakter, discipline en klas moesten we de blaadjes onder elkaar verdelen en zo teams maken.

Nadat ik kennis had gemaakt met mijn teamgenoten gingen we elkaar beter leren kennen. De vorige week moesten we een foto maken van een plek die waarde heeft voor ons. Hier moesten we met elkaar praten en vragen stellen over wat er werd verteld. Door dit te doen hadden we de mogelijkheid elkaar beter te leren kennen en konden we elkaars waarden en behoeftes ontdekken.

Op basis van de opgeschreven waarden hebben we een teamuitje verzonnen. Door de waarden zelfstandigheid, sociaal en avontuurlijk zijn we op het plan gekomen om naar een Korean barbecue te gaan omdat we hier allemaal nog niet zijn geweest. Nadat we een dag hadden afgesproken zijn we naar huis gegaan.

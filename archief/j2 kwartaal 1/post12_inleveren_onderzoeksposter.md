---
title: Inleveren Onderzoeksposter
date: 2018-09-28
---

De onderzoeksposter moest vandaag ingeleverd worden op school. In principe zijn we heel de middag bezig geweest met het printen hiervan. De printer in het stadslab, de enige printer op school met A1 formaat, was stuk gegaan. Daarom moesten we naar WdKa om onze poster te laten printen. Hier was een wachtrij dus hebben wij anderhalf moeten wachten.

In de tussentijd heb ik ook feedback gevraagd op het feedbackformulier dat ik heb gemaakt voor Woonstad. Ik kreeg als opmerking van Noa dat ik beter open vragen kan stellen i.p.v. meerkeuze. Hierdoor kan ik uitgebreidere feedback krijgen i.p.v. dat er snel wat aangevinkt wordt. Ik vond dit goede feedback, dus heb ik dit dan ook verwerkt in de uiteindelijke versie van het document.

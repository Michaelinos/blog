---
title: Terugblik Kwartaal 1
date: 2018-10-22
---

Kwartaal 1 was voor mij weer even inkomen na een lange vakantie. In het begin had ik wat moeite met het leidinggeven in de groep. Dat kwam ook wel omdat het een diverse groep was met mensen die elkaar allemaal nog niet zo goed kenden. In het begin van het project hadden we een samenwerkingscontract gemaakt, hierdoor hadden we een aantal regels en gezamenlijke wensen vastgesteld, wat onze samenwerking in de loop van het project heeft verbeterd.

Na de initiële briefing van Woonstad ben ik aan de slag gegaan met alle verzamelde informatie om te zetten naar een eigen debriefing. Hierdoor was de opdracht voor het team en mij duidelijk en wisten we waar we moesten beginnen met ons onderzoek. We hebben gezamenlijk allerlei onderzoeksrichtingen samengesteld waar we mee aan de slag konden gaan. Ik heb hierbij deskresearch gedaan naar de doelgroep sociale huurders, hierdoor had ik wat meer informatie over wat hun bezighoudt, waar ze in Rotterdam wonen en hoe tevreden ze zijn over Woonstad.

Ook heb ik een ochtend meegelopen met een huismeester in de wijk Prinsenland, in Rotterdam Noord-Oost. Hierdoor had ik wat meer inzicht in de omgang tussen de bewoners en Woonstad. Ook heb ik door het gesprek met de huismeester veel inzichten kunnen halen over de problemen bij Woonstad en wat voor problemen de bewoners ervaren. Helaas heb ik tijdens het meelopen niet veel bewoners kunnen spreken omdat we maar bij één melding zijn geweest. Daarom heb ik verschillende laddering interviews gehouden om wat meer inzicht te krijgen in de kernwaarden en normen van onze doelgroep.

Toen we eenmaal redelijk wat informatie hadden verzameld, zijn we begonnen met onze inzichten te verzamelen. Ik heb dit gedaan door het maken van een customer journey en een persona. Door dit te maken konden we tijdens onze ideevorming makkelijk refereren naar deze deliverables om onze kansen en knelpunten in te zien en hier rekening mee te houden tijdens het bedenken van een concept. Alle verzamelde informatie hebben we dan ook verwerkt in een onderzoeksposter en deze bij Woonstad ingeleverd.

Na ons onderzoek hebben we uiteindelijk gekozen om te focussen op de doelgroep oudere mensen boven de 50+ jaar woonachtig in een sociale huurwoning van Woonstad. Hier hebben wij voor gekozen omdat wij hier de grootste kans zagen voor verbetering in de klantrelatie. Hier heb ik dan ook de debriefing op aangepast zodat wij verder konden gaan met de ideevorming. Door het gebruik van online toolkits heb ik een aantal leuke creatieve technieken kunnen vonden die mijn team en ik hebben gebruikt om een aantal concepten te verzinnen. Door meerdere malen te divergeren en convergeren zijn we uiteindelijk gekomen bij drie concepten:

-	Een control panel in huis
-	Een community van bewoners (Leefstad Rotterdam)
-	Een virtuele assistent (hologram)

Deze concepten hebben we doormiddel van een presentatie voorgelegd aan een medewerker van Woonstad. Hier hebben wij als feedback gekregen dat zij Leefstad Rotterdam een interessant concept vindt om verder over na te denken en hoe we dit haalbaar kunnen maken. Ook lijkt het haar een goed idee om de doelgroep van dit concept breder te maken. Er zijn meerdere groepen mensen waar dit concept interessant voor kan zijn.

Al met al hebben we in het eerste kwartaal goede voortgang gemaakt en hebben wij een aantal richten die wij verder kunnen onderzoeken, uitwerken en testen. Ook heb ik veel geleerd over hoe ik het ontwerpproces kan aanpakken en hoe ik het team moet leiden. Ik ben dan ook benieuwd wat wij het volgende kwartaal kunnen bereiken met dit project.

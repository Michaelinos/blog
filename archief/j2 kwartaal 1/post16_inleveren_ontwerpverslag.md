---
title: Inleveren Ontwerverslag
date: 2018-10-05
---

In de middag hebben we weer als team ontmoet. Ik heb de laatste puntjes op de i gezet bij het ontwerpverslag en alles gecontroleerd. Alle deliverables staan er nu in en ik heb het document ingeleverd. Ook hebben wij besproken welke doelgroep wij nou willen kiezen en hoe we het verdere onderzoek gaan aanpakken volgende week. We hebben uiteindelijk gekozen voor 50+ers omdat wij hier een kans zien het klantcontact te verbeteren. We hebben besloten dat volgende week een deel van het team naar een ouderenverzorgingstehuis en een deel naar een paar wijken in Rotterdam gaat om de doelgroep te interviewen. Later op de week gaan we aan de hand van deze resultaten een aantal concepten verzinnen.

---
title: Groepsuitje & Onderzoeksvragen
date: 2018-09-11
---

Gisteren zijn we met het projectgroepje naar een Korean barbecue geweest, wat erg gezellig was. Deze ochtend heb ik samen met Jari een korte presentatie gegeven over het teamuitje. Hier hebben we wat foto’s laten zien en verteld waarom we voor deze plek hebben gekozen voor ons uitje.

Vervolgens hebben we uitleg gekregen over hoe we nu aan de slag moeten met het project. Allereerst hebben we een lijst gemaakt met wat interessant is om te onderzoeken en wat niet. Hierdoor wisten we waar we ons op moesten focussen met het onderzoek en hadden we een aantal interessante categorieën. Hierna zijn ik en de andere teamleiders apart genomen om wat te vertellen over de voortgang van het project. Hier heb ik vertelt dat we teamafspraken hebben gemaakt en een Trello en Dropbox hebben opgezet.

In de middag kregen we wat uitleg over de verschillende labs en in welk lokaal ze te vinden zijn. Ik heb besloten dat ik naar het user research lab ga omdat het erg nuttig is in dit stadium van het project. Ook heb ik met mijn team afspraken gemaakt over opdrachten. Jari en Lorenzo maken onderzoeksvragen voor twee onderzoek categorieën. Josephin maakt de planning en ik maak de debriefing. Na de afspraken was het tijd voor het user research lab. Hier heb ik uitleg gekregen over hoe je een onderzoek moet aanpakken en wat voor verschillende onderzoeksmethoden zijn. In deze les zijn we ook begonnen met het maken van een onderzoeksplan.

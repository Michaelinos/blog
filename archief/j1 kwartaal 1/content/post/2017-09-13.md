---
title: Woensdag 13-9-17
date: 2017-09-13
---

Vandaag hebben we een briefing gekregen over wat we vrijdag allemaal moeten inleveren voor onze individuele en gezamenlijke opdrachten. Hierna zijn we begonnen met het maken van een paper prototype voor ons project.

Iedereen is individueel begonnen met schetsen van startschermen, registratie etc. Hierna hebben we deze van elkaar vergelijkt en verschillende ideeën gecombineerd om een soort flow in het gebruik van de app te creëren. Het hele registratie proces hebben we op deze manier uit kunnen werken vandaag. Ook hebben we een begin gemaakt aan leaderboards en het startscherm.

Vervolgens was het tijd voor de lessen van onze studiecoach. Hier hebben we uitleg gekregen over het dit-ben-ik profiel dat we moeten maken. Ook moeten we voor dit profiel de Belbin en Kolb test afleggen om onze persoonlijkheid/leermanier te ontdekken. Dit hebben we in deze les ook gedaan. Hierna waren we klaar vandaag.
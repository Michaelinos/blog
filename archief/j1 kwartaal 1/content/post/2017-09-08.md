---
title: Vrijdag 8-9-17
date: 2017-09-08
---

Vandaag zijn Valentino, Jordi en Philip naar de Willem de Koning Academie gegaan om studenten van de opleiding Grafisch Design te interview. Dit is onze doelgroep voor ons project. Er zijn in totaal 4 interviews afgelegd waar een audiofragment van is opgenomen en een document is geschreven. 

Er wordt van het weekend een eindverslag geschreven over de interviews. Uit dit verslag kunnen we conclusies trekken voor ons thema en het spel dat we willen gaan kiezen.

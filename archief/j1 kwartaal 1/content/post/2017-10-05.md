---
title: Donderdag 5-10-2017
date: 2017-10-05
---

Vandaag heb ik alleen een nulmeting gehad voor de vakken Nederlands en Engels in de digitale toets ruimte. De Engelse toets ging voor mijn gevoel erg soepel. Met de Nederlandse toets had ik wat meer moeite, dit komt omdat ik de D en T regels niet goed weet.

Na de toetsen ben ik naar huis gegaan, we hadden vandaag geen les meer.
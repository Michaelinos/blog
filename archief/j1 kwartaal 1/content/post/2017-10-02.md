---
title: Maandag 2-10-17
date: 2017-10-02
---

In de ochtend hebben we allereerst verschillende spelanalyses klassikaal besproken. Waar het in principe op neerkomt is dat je uitleg uitgebreid moet zijn en je bevindingen goed moet onderbouwen. Vervolgens werd on vertelt hoe laat we woensdag ons project moeten presenteren. Na deze uitleg zijn we heel de dag verder gaan werken aan ons project.

We hebben de taken verdeeld en we zijn aan de slag gegaan. Ik heb verschillende dingen gedaan vandaag.

-	Ontwerpproces van iteratie 1 en 2 samen met Jordi beschreven en uitgewerkt
-	Concept in klad uitgewerkt voor ons prototype
-	Gebruikte creatieve technieken beschreven in een document
-	Groepschat aangemaakt met onze doelgroep en om feedback gevraagd
-	Speluitleg afgemaakt
-	Alle documentatie die we tot nu hebben verwerkt en samengevoegd in 1 document
-	Beroepsprofiel grotendeels afgemaakt

Voortgang besproken met mentor en hierbij ook aangegeven dat we het nogal vervelend vinden dat Valentino en Simon regelmatig te laat komen waardoor we achterlopen met het project. Dit vervolgens met Valentino en Simon besproken toen hun kwamen, ze zeiden dat ze hieraan gaan werken.

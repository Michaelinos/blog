---
title: Cultural Probe Verder Uitwerken
date: 2019-03-07
---

Emma heeft gisteren alvast een aantal goede schetsen gemaakt voor de uitwerking van de cultural probe. Aan de hand van deze schetsen zijn Leroy en Emma bezig geweest met de indeling van de probe. Leroy regelde de visuele uitwerkingen en waar nodig heb ik hem hierbij geassisteerd. Verder heb ik een introductiepagina uitgewerkt in de stijl die wij willen gaan hanteren. In de middag ben ik samen met Leroy de stad ingegaan om te kijken wat we gaan kopen voor de presentatie van onze cultural probe. Bij de Action zagen wij hier een aantal leuke dingen voor. Ook heb ik een aantal keer wat pagina’s uitgeprint om te kijken hoe dit er uiteindelijk uit gaat zien. Aan het einde van de middag is het ons gelukt om een eerste prototype van onze probe uit te printen en bruikbaar te maken. Dit heeft Jari meer naar huis genomen om te testen met zijn moeder. 

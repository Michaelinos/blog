---
title: Brainstormen Concept
date: 2019-03-28
---

Vandaag hebben we vroeg afgesproken om met elkaar te brainstormen. Het doel was om vandaag drie verschillende concepten te verzinnen. Ten eerste hebben we alle drie verschillende creatieve technieken verzameld en deze met elkaar gedeeld. De beste drie kiezen we uit om te gebruiken. Uiteindelijk hebben we gekozen voor download your learnings, negatief brainstormen, brainwriting, mindmapping, COCD-box en MoSCoW-analyse. 

We zijn begonnen met download your learnings om al ons onderzoek te dumpen in verschillende categorieën en dit fysiek in de wereld te hebben. Vervolgens hebben we brainwriting gedaan en ten slotte negatief brainstormen. We zijn hiermee tot veel verschillende concepten gekomen die we vervolgens met een COCD-box hebben geconvergeerd. Uiteindelijk hebben we gekozen voor een smartwatch, expierence en locatie prototype.

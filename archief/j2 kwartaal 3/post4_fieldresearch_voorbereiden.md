---
title: Fieldresearch Voorbereiden
date: 2019-03-05
---

Na de presentaties over de teamuitjes zijn we aan de slag gegaan met het bedenken hoe we de doelgroep kunnen onderzoeken/ondervragen. Eerst hebben we onszelf als doelgroep gepakt en vragen over hoe wij nieuws consumeren beantwoord. Na wat ideeën kwam we uiteindelijk op het idee om een cultural probe toe te passen. Het idee is om een paar testpersonen te vinden die voor ons een week lang een dagboekje bij willen houden over wanneer, hoe en welk nieuws zij lezen en/of bekijken. Om zo erachter te komen hoe onze doelgroep zich op verschillende manieren bewust maakt van het nieuws op hem/haar heen.

Nadat we het idee hadden uitgeschreven hebben we hierover feedback gevraagd bij Ellen. Zij vond het een goed idee en heeft nog wat tips gegeven om over na te denken qua vraagstelling. Ook hebben we besloten dat we in eerste instantie een prototype gaan maken van de cultural probe en deze gaan testen met één van onze ouders. Aan de hand van deze feedback kunnen we onze cultural probe verder verbeteren voordat wij hiermee verder gaan onderzoeken.

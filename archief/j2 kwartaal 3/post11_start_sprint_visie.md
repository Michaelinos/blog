---
title: Start Sprint 2 + Visie
date: 2019-03-26
---

Vandaag zijn we begonnen met een nieuwe sprint en hebben we dus ook een nieuwe briefing gekregen. Deze briefing hebben we met ons groepje doorgenomen. Vervolgens zijn Emma en ik aan de slag gegaan met een nieuwe planning te maken in Trello a.d.h.v. van deze briefing. Hierna kwamen erachter dat we met ons groepje nog niet echt een goede visie hadden, dus hebben we deze bijgewerkt. Deze hebben we dan ook aan Ellen voorgelegd voor feedback.

Feedback: Ik hoor in jullie visie het woord onbewust erg naar voren komen, en dan vooral onbewust personalisering. Willen jullie de mensen onbewust gepersonaliseerd nieuws laten lezen? In hoeverre is dat ons doel is en of wij hier ook van weten dat de doelgroep het op deze manier wilt. Verder vond ze de visie nog niet helemaal af, wat wij nu echt willen gaan aanpakken was nog niet duidelijk volgens haar.
- Omdat wij nog niet echt ons hadden verdiept in het onderzoeken van personalisatie bij onze doelgroep zijn we meteen aan de slag gegaan om hierover vragen op te stellen en deze in de vorm van een interview kunnen afnemen. De visie gaan we hierna opnieuw opstellen, als we beter inzicht hebben. Tevens hebben we afgesproken dat iedereen 3 creatieve technieken gaat opzoeken wat we a.s. donderdag met elkaar gaan bespreken en de beste uitkiezen.

Omdat we nog geen onderzoek hadden gedaan naar personalisatie bij de doelgroep, hebben we besloten dit alsnog te doen. Emma heeft op voorhand een aantal interviewvragen hiervoor opgesteld. Ik heb in de avond met een vriend van me afgesproken om hem over personalisatie bij het nieuws te interviewen. Dit is goed gegaan, ik heb bruikbare antwoorden kunnen verkrijgen. Dit heb ik dan ook verwerkt zodat ik deze resultaten donderdag kan delen met de rest.

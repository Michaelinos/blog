---
title: Presentatie Opdrachtgever
date: 2019-02-22
---

Vandaag was er door de opdrachtgever een presentatie georganiseerd op school. Dit was eigenlijk een soort uitleg over waar het NRC zich momenteel voor inzet en wat hun doel is met de opdracht. Hierna was er tijd voor een Q&A-ronde. Hierbij heb ik een paar vragen gesteld en werden er nog veel meer andere vragen beantwoord. Dit alles heb ik genoteerd en verwerkt en in de middag met de rest van het team gedeeld.

In de middag hebben Jari, Leroy en ik samen een SWOT gemaakt en verder toegelicht met elkaar. Op basis van deze resultaten hebben wij met elkaar teamafspraken gemaakt waar iedereen tevreden mee was.
Op het einde van de middag hebben we met elkaar afgesproken dat Leroy en ik maandag even samen komen om een aantal fieldresearchtechnieken voor te bereiden voor a.s. donderdag.

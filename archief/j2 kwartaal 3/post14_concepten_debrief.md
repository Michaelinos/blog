---
title: Concepten Uitwerken + Debrief Feedback
date: 2019-04-04
---

We hebben dus besloten om voor drie concepten een prototype uit te werken. NRC locatie wordt gepresenteerd door een storyboard, NRC experience doormiddel van een moodboard en NRC Mood/playlist doormiddel van een low-fid clickable prototype. Met deze laatste ben ik aan de slag gegaan. Ik heb allereerst een paar grove schetsen gemaakt op papier om te bepalen wat voor schermen ik wil maken en hoe ik deze ga indelen. Dit heb ik vervolgens met Adobe XD uitgewerkt tot een simpel prototype. Hier ben ik eigenlijk heel de ochtend mee bezig geweest, ik heb het resultaat laten zien en de rest van de groep en zij zijn er tevreden over. Ik heb alleen een paar kleine aanpassingen gemaakt om het prototype verder te verhelderen.

Sinds ik afgelopen maandag een debriefing had gemaakt, wilde ik hier graag feedback over. Jari heeft naar mijn debriefing gekeken en heeft het ontwerpdoel en probleemsituatie verbeterd en mij uitgelegd waarom hij deze aanpassingen heeft gemaakt. Dit heeft mij geholpen waardoor we een betere debriefing hebben gekregen. In de avond heb ik de feedback op het ontwerpverslag verwerkt in de stukken tekst die ik heb geschreven.

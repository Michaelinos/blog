---
title: Ontwerpverslag afmaken + opdrachtgever presentatie
date: 2019-04-05
---

Vandaag was het weer tijd om een nieuwe versie van het ontwerpverslag op te leveren. Ik heb weer de taak op me genomen om alles samen te voegen, waar ik de hele ochtend mee bezig was.

In de middag hebben wij onze drie conceptrichtingen gepresenteerd aan de opdrachtgever. Leroy en Jari zijn bezig geweest hier visualisaties voor te maken en uit te printen. Een half uur voor de presentatie hebben we alles nog even doorgenomen en met elkaar geoefend waardoor de presentatie soepeltjes is verlopen. De opdrachtgever was voornamelijk enthousiast over NRC Experience en NRC Mood. Hij vond het uniek dat wij voornamelijk hebben ingezet op de emotie van de lezer en is dan ook benieuwd hoe we hier mee verder gaan in de aankomende design sprints.

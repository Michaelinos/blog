---
title: Deskresearch AI
date: 2019-03-11
---

Ik had vorige week een aantal onderzoeksvragen opgesteld voor mijn deskresearch naar AI:

-	Gebruikt NRC momenteel AI/Machine Learning technologie?
-	Wat voor toepassingen zijn mogelijk voor personalisatie voor de eindgebruiker?
-	Wordt AI/Machine Learning momenteel gebruikt door concurrerende bedrijven? Zo ja, op wat voor manier wordt het toegepast?
-	Hoe kan er voorkomen worden dat men in een filterbubbel raakt door AI-personalisatie?
-	Wat is de conclusie? Op wat voor manier kan NRC gaan implementeren?

Aan de hand van deze onderzoeksvragen ben ik vandaag aan de slag gegaan om deze te beantwoorden. Hierbij heb ik veel verschillende bronnen en nieuwswebsites bezocht en deze met elkaar vergeleken om een zo’n compleet mogelijk resultaat te krijgen. De rest van de week heb ik niks gedaan aan het project i.v.m. de studiereis naar Berlijn.

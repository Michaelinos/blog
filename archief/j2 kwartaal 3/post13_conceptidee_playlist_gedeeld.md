---
title: Conceptidee Playlist Gedeeld
date: 2019-04-01
---

Van het weekend lag ik in bed en dacht ik plotseling aan een nieuwe conceptidee. Verschillende nieuws playlists voor het NRC waarbij het nieuws steeds verder wordt gepersonaliseerd hoe vaker je de playlist gebruikt. Ik heb dit conceptidee vandaag met de rest gedeeld en zij zijn enthousiast over het idee. Het kan veel gevarieerde playlists bevatten en de personalisatie komt hier ook in terug. We hebben voor dit concept gekozen i.p.v. de smartwatch en we hebben een mindmap gemaakt voor alle concepten. Emma en Jari hebben hier volgens conceptbeschrijvingen voor gemaakt en een golden circle toegepast met de concepten.

Ook ben ik vandaag aan de slag gegaan met het maken van een debriefing, want deze hadden we nog niet gemaakt.

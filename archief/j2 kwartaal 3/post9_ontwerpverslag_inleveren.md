---
title: Ontwerpverslag Inleveren
date: 2019-03-21
---

Vandaag stond in het teken van het ontwerpverslag. De deadline was eigenlijk morgen, maar we wilden eigenlijk graag vandaag het verslag ingeleverd hebben. Ik heb de taak op mij genomen om dit verslag verder af te maken en al ons onderzoek te bundelen en categoriseren. Ook heb ik het document verder opgemaakt en gezorgd voor consistentie in de lay-out. Hier ben ik eigenlijk heel de ochtend en middag mee bezig geweest.

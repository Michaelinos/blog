---
title: Terugblik Kwartaal 3
date: 2019-04-11
---

Dit kwartaal zit ik weer in een compleet nieuw team. Natuurlijk weer effe wennen maar de samenwerking verliep over het algemeen heel soepel. Dit groepje is wat meer serieus en daardoor hebben we veel kunnen verrichten in een korte tijd.

Ik ben begonnen met het bedenken van een aantal onderzoeksrichtingen voor ons project met het NRC. Ik wilde graag wat meer weten over nieuwsconsumptie in Nederland en hoe personalisatie momenteel wordt ingezet door het NRC/concurrentie. Daarom heb ik ten eerste deskresearch gedaan naar het NRC a.d.h.v. een aantal onderzoeksvragen.

Na de deskresearch ronde ben ik begonnen met fieldresearchtechnieken voor te bereiden. Uiteindelijk hebben we gekozen voor een cultural probe. Na een aantal mock-ups hebben we zes verschillende testpersonen een week lang onze cultural probe laten gebruiken. We hebben hiermee interessante testresultaten verzameld en hier conclusies uitgetrokken. Alleen miste we met dit onderzoek hoe de doelgroep dacht over personalisatie in het nieuws. Daarom hebben we nog een enquête georganiseerd en hebben we een aantal ongestructureerde interviews gehouden met de doelgroep. Hierdoor hebben we een completer beeld kunnen krijgen over de doelgroep. Wel ontbrak er voor mijn gevoel deskresearch over hoe personalisatie d.m.v. machine learning wordt ingezet door concurrerende bedrijven. Daarom heb ik hier zelf nog extra deskresearch naar verricht en goede inzichten uitgehaald voor ons concept.

Na al ons onderzoek was het tijd om onze onderzoeksresultaten te presenteren aan de opdrachtgever. Dit hebben we gedaan d.m.v. een researchposter. Iedereen heeft hierbij een stukje gepresenteerd over ons onderzoek. De opdrachtgever had niet echt tijd om uitgebreide feedback te geven op ons onderzoek maar hij was benieuwd naar wat deze onderzoeksresultaten ons gaan opleveren bij het bedenken van concepten.

Dus ons onderzoek was nu afgerond, het was nu tijd om concepten te verzinnen. We zijn begonnen met download your learnings om al ons onderzoek te dumpen in verschillende categorieën en dit fysiek in de wereld te hebben. Vervolgens hebben we brainwriting gedaan en ten slotte negatief brainstormen. We zijn hiermee tot veel verschillende concepten gekomen die we vervolgens met een COCD-box hebben geconvergeerd. Uiteindelijk hebben we gekozen voor een smartwatch, expierence en locatie prototype.

Ik dacht plotseling aan een nieuw concept. NRC Mood. Verschillende nieuws playlists voor het NRC waarbij het nieuws steeds verder wordt gepersonaliseerd hoe vaker je de playlist gebruikt. Ik heb dit conceptidee met de rest gedeeld en zij zijn enthousiast over het idee. Ik heb dit concept verder uitgewerkt in een low-fid prototype d.m.v. Adobe XD. Dit concept is dan in de plaats van de smartwatch gekomen omdat deze niet heel realistisch haalbaar is.

Vervolgens hebben wij onze drie conceptrichtingen gepresenteerd aan de opdrachtgever. Een half uur voor de presentatie hebben we alles nog even doorgenomen en met elkaar geoefend waardoor de presentatie soepeltjes is verlopen. De opdrachtgever was voornamelijk enthousiast over NRC Experience en NRC Mood. Met deze concepten willen we inzetten op de emotie van de gebruiker. Waarschijnlijk gaan we verder met het concept NRC Mood. Ik ben dan ook benieuwd hoe we dit verder gaan uitwerken in de komende design sprints.

---
title: Dinsdag Prototype party + concept feedback
date: 2019-06-11
---

Vandaag hadden wij in de ochtend een prototype party. Dit was een moment om samen met andere klassen elkaars concepten te beoordelen. We hebben een feedbackformulier gemaakt en ik heb zelf twee tests afgenomen met mede studenten. Het is teveel feedback om hier in op te nemen maar tijdens deze feedbackronde hebben mijn team en ik opgemerkt dat wij toch tegen beperkingen aan lopen binnen Adobe XD. De animaties zijn mager en niet alles werkt even soepel. Daarom hebben wij in het kader van professionaliseren besloten om ons prototype verder te gaan maken in Framer. Op deze manier kunnen wij een nieuw veelgebruikt pakket leren en zijn er ook meer mogelijkheden voor prototyping tot onze beschikking.

Ook merkte een aantal mensen op dat niet al onze categorieën een emotie aanspreekt, daarom hebben wij dit omgegooid en in een brainstormsessie nieuwe categorieën gemaakt.

Bij creative concepting lab heb ik feedback gevraagd over ons concept en hoe wij dit sterker neer kunnen zetten. Dit heb ik gedaan aan de hand van een aantal voor opgestelde criteria.

Tijdens deze feedback ronde heb ik een korte pitch gegeven over ons concept, uitgelegd op welke ontwerpcriteria dit is gebaseerd en heb ik ons prototype laten zien. Vervolgens hebben Celine Duindam en Wessel, een klasgenoot, het concept van ons groepje beoordeeld aan de hand van een voor opgesteld formulier.

Deze beoordeling is te categoriseren in vier onderwerpen. Celine Duindam heeft deze feedback samengevat:

What is

Je beschrijft duidelijke ontwerpprincipes alleen je blijft hier nog wel oppervlakkig. Probeer hier de volgende keer dieper op in te gaan. Je inzichten zijn wel heel interessant om hier verder op te bouwen.

What if

Je communiceert je visie niet duidelijk en laat dit merken. Je weet waar je het over hebt, bouw de spanning op in je verhaal!

What wow

Het concept is interessant, maar met emotie zou je nog veel meer mee kunnen doen, dit zou het concept op langere termijn interessanter maken. Mag je als tip of idee meegeven.

What works

Let op dat je niet het hele merk NRC weggooit of je moet alles goed kunnen onderbouwen.

Dit feedbackmoment heeft mij toch wel even wakker geschud over de stand van zaken van ons project. Na dit lab heb ik gelijk een berichtje naar de rest van mijn team gestuurd om deze feedback te delen. Zij waren het er mee eens dat wij ons concept sterker neer moeten zetten, bijvoorbeeld door een koppeling met een smart device om emotie real time te kunnen meten. Ook heb ik besproken dat onze huidige huisstijl niet goed past bij NRC, waardoor we weer terug naar de NRC lettertypes zijn gegaan. Vervolgens heb ik al ons huidige onderzoeken gebundeld in een testrapportage zodat al onze inzichten netjes bij elkaar staan.

---
title: Testdag prototype
date: 2019-06-04
---

Mijn kleuren prototype heb ik vandaag aan de rest van het team laten zien en zij ook de prototypes die hun hebben gemaakt. Vervolgens hebben wij besproken hoe wij het gaan testen. We maken een gezamenlijk testplan met een aantal specifieke vragen voor ieder prototype. Vervolgens laten wij per testpersoon één prototype zien totdat ieder prototype twee keer is getest.

In de middag ben ik naar creative concepting lab gegaan, hier ben ik samen met Leroy bezig geweest met het bedenken van een media press kit voor de eindexpositie. We hebben dan ook een motto bedacht: “Voor ieder moment & ieder gevoel” en tevens hebben wij verschillende schetsen gemaakt voor het logo van NRC Mood, ons concept.

---
title: Terugblik kwartaal 4
date: 2019-06-25
---

We hebben dit kwartaal besloten om verder te gaan met NRC Mood. Een verlengstuk van het NRC dat een koppeling maakt tussen emotie en nieuws. We hebben dan ook een conceptposter gemaakt om ons idee presenteer te kunnen maken aan Ijsbrand. Hierbij heb ik samen met Leroy het visuele aspect van deze poster gemaakt.

Ijsbrand heeft ons veel goede feedback gegeven en vindt emotie een interessante invalshoek voor het concept. Het was nu dan tijd om verder in de diepte te gaan met ons concept. Samen met Emma heb ik dan ook ontwerpcriteria gemaakt op basis van onze inzichten. Dit is: transparantie boven alles, houd mij op de hoogte, geef mij de controle, snel, niet te veel moeite en verras mij

Op dit moment ervaarde ik wel dat ik niet zo goed wist hoe ik emotie nou verder moest onderzoeken. Hier hadden we allemaal last van. We zijn daarom naar Marije gegaan om feedback te vragen over hoe we dit kunnen aanpakken. Uiteindelijk hebben we besloten dat we deskresearch moeten verrichten naar emotie. Daarom heb ik dan ook een boek doorgespit over emotional design en heb ik deskresearch gedaan naar kleur en emotie.

Jari heeft een lat gemaakt met het kleurenspectrum en een aantal artikelen verzameld die een emotie aanspreken. Vervolgens hebben wijj een aantal testpersonen een stapel artikelen gegeven en gevraagd of zij dit willen categoriseren per kleur en wat voor emotie zij aan deze kleur zouden koppelen.

Deze tests hebben wij op de Hogeschool Rotterdam gehouden en in de binnenstad van Rotterdam. In totaal hebben wij vijf tests afgenomen. Deze zijn soepel verlopen en gedocumenteerd en wij hebben er wel een aantal interessante gegevens uit kunnen halen.

De volgende dag was er een design crit op school. Hier hadden wij de mogelijkheid om feedbak te krijgen op ons concept van oud CMD studenten.

Per feedback ronde heb ik een korte uitleg gegeven over ons onderzoek, wat uit ons onderzoek is gekomen en hoe wij dat hebben verwerkt in een concept. Vervolgens heb ik uitgelegd wat het concept precies inhoud en wat de mogelijke functies zijn.

Aan de hand van deze feedback hebben wij besloten drie verschillende conceptrichtingen uit te werken die allemaal te maken hebben met emotie aanspreken: Tone of voice, kleur en typografie.

 
Ik heb de taak op mij genomen om een prototype te maken omtrent kleur. Dit is goed ontvangen en uiteindelijk hebben we besloten dit verder uit te werken als ons hoofd concept. Na verschillende iteraties en tests hebben mijn team en ik opgemerkt dat wij toch tegen beperkingen aan lopen binnen Adobe XD. De animaties zijn mager en niet alles werkt even soepel. Daarom hebben wij in het kader van professionaliseren besloten om ons prototype verder te gaan maken in Framer. Op deze manier kunnen wij een nieuw veelgebruikt pakket leren en zijn er ook meer mogelijkheden voor prototyping tot onze beschikking.

Ook kwam ik er door CC lab achter dat we niet alles uit het concept hebben kunnen halen wat mogelijk is. Ik heb dit met de rest van het team besproken en zij waren het er mee eens. Daarom hebben we nog een keer al ons onderzoek op een rijtje gezet en hebben we een smartdevice toegevoegd aan het prototype.

Na weer een aantal iteraties was het prototype eindelijk klaar en de eindexpositie in zicht.

Hier heb ik een aantal keer ons onderzoek en het concept gepresenteerd en uiteraard het prototype laten zien. We hebben veel goede feedback ontvangen, iedereen vond onze focus op emotie een interessante invalshoek.

Ook hebben wij feedback gekregen van Marije en Ellen:

Ze waren blij verrast. Ze vonden het consequent uitgevoerd en Marije was blij met het eindproduct. Doordat we minder tijd hadden en uiteindelijk pas laat de beslissing hadden gemaakt op wat wij wilden focussen, zorgde dat ervoor dat we er niet alles uit hebben kunnen halen. Dit had anders geweest als we extra tijd hadden. Al om al vonden ze het een mooi eindproduct.

Ik ben dan ook blij met het eindresultaat dat wij hebben geleverd. Ik denk wel dat we er meer uit hadden kunnen halen door een strakkere planning en als we eerder concrete stappen hadden met hoe wij ons concept wilden gaan uitdiepen. Natuurlijk is dat makkelijker gezegd dan gedaan. Emotie is een interessante richting maar ook erg subjectief en moeilijk te onderzoeken.

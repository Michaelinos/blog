---
title: Prototype werkzaamheden
date: 2019-06-23
---

Vandaag waren de laatste lootjes voor het prototype. Ik heb alle gevonden problemen opgelost en het prototype compleet functionerend gemaakt.

In de avond heb ik mijn stukken voor de ontwerpdocumentatie geschreven: positionering van het concept, het kleuren prototype en de feedback van de design crit.

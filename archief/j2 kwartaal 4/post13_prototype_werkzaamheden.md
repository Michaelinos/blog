---
title: Prototype werkzaamheden
date: 2019-06-09
---

Aan de hand van de eerder verzamelde feedback heb ik een nieuwe iteratie van het prototype gemaakt. Zo heb ik de kleuren per afspeellijst afgestemd op ons kleurenonderzoek en is het één geheel geworden. Ook heb ik de navigatie verbeterd tussen artikelen in een afspeellijst. Ook zijn de navigatieknoppen onderaan de artikelen vervangen.

Ook ben ik in Adobe XD bezig geweest om een nieuwe manier van navigeren tussen artikelen te maken. Ik heb hier wat filmpjes van gekeken en een template gemaakt die ik heb gedeeld met de rest van het team.

Tevens ben ik bezig geweest om mijn eigen customer journey en persona te maken van de huidige NRC doelgroep.

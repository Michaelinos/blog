---
title: Eindexpositie
date: 2019-06-25
---

Vandaag hadden we dan de eindexpositie. Hier heb ik een aantal keer ons onderzoek en het concept gepresenteerd en uiteraard het prototype laten zien. We hebben veel goede feedback ontvangen, iedereen vond onze focus op emotie een interessante invalshoek.

Ook hebben wij feedback gekregen van Marije en Ellen:

Ze waren blij verrast. Ze vonden het consequent uitgevoerd en Marije was blij met het eindproduct. Doordat we minder tijd hadden en uiteindelijk pas laat de beslissing hadden gemaakt op wat wij wilden focussen, zorgde dat ervoor dat we er niet alles uit hebben kunnen halen. Dit had anders geweest als we extra tijd hadden. Al om al vonden ze het een mooi eindproduct.

---
title: Voorbereidingen expo
date: 2019-06-24
---

Vandaag stond in het teken van de voorbereidingen voor de expositie morgen. We hebben alle presentatiematerialen afgedrukt, ik heb een poster van de ontwerpcriteria gemaakt en ik heb feedback gegeven over de conceptposter van Emma. Aan het eind van de dag hebben we ook samen met een andere groep onze pitch geoefend en elkaar feedback gegeven.

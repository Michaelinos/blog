---
title: Presentatie Ijsbrand
date: 2019-05-10
---

Vrijdag hebben wij in het kort uitgelegd waar ons concept over gaat en hoe wij dit het beste kunnen presenteren bij de eindexpositie.

Feedback Ijsbrand:

-	Je zou je pitch goed kunnen beginnen met: Wij willen de gebruiker van NRC toespitsen met berichten gebaseerd op emoties die de gebruiker ondergaat, zoals uit ons onderzoek is gebleken (onderzoek naar emotie). Dit zorgt ervoor dat de berichten gerichter aankomen.
-	Een idee om een verplichten shuffle toe te passen wanneer een gebruiker te veel van hetzelfde leest.
-	Om jou als lezer moeten wij meer over jou weten. En gaan we je goed uitleggen waarom en hoe (dit wekt vertrouwen bij de lezers).
-	Lees elke dag de NRC, als aanbeveling. Zodat je er meer vanaf weet.
-	Hoe weet NRC waar ik ben? (door GPS zeiden wij) Maak dit heel transparant! Geef de gebruiker een uitgebreide uitleg over hoe jullie te werk gaan. En elke dag toestemming geven misschien?

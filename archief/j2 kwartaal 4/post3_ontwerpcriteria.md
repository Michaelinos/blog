---
title: Ontwerpcriteria
date: 2019-05-14
---

Leroy heeft aangekaart dat wij nog geen ontwerpcriteria hebben opgesteld van onze onderzoeksresultaten, daarom hebben wij met z’n alle ons onderzoek doorgespit om hier criteria uit op te stellen. Emma en ik zijn bezig geweest om ontwerpcriteria op te stellen voor de doelgroep. Uiteindelijk zijn wij gekomen op:

-	Transparantie boven alles
-	Houd mij op de hoogte
-	Geef mij de controle
-	Snel, niet te veel moeite
-	Verras mij

In de middag heb ik creative concepting lab gehad, hier ben ik bezig geweest met een business model te maken voor het concept. Hier heb ik o.a. het verdienmodel en de selling points van ons concept besproken.

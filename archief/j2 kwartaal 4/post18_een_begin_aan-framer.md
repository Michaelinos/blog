---
title: Een begin aan framer
date: 2019-06-17
---

Samen met Leroy ben ik begonnen met het maken van het prototype in Framer. Ik kan geen ontwerpen vanuit Adobe XD importeren in Framer, dus hebben wij eerst alle schermen over moeten zetten naar Sketch. Nadat dat gebeurd was, hebben wij een brainstormsessie gehouden over de benamingen van de categorieën die veel meer een emotie aanspreken. Uiteindelijk is dit geworden:

1.	Speciaal voor jou(Goud)
2.	Fleur me op (Paars)
3.	Prikkelend (Groen)
4.	Intens (Rood)
5.	Verbazingwekkend (Cyaan)
6.	Ontroerend (blauw)

Vervolgens zijn we begonnen met prototyping in Framer. Ik heb een aantal video’s gekeken en ik had nu een redelijk idee over hoe het allemaal werkt. Het pakket zelf was niet heel erg snel, maar ik heb de werking wel snel op kunnen pakken. Hier ben ik dan ook de rest van de dag mee bezig geweest.

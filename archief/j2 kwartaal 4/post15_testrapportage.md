---
title: Testrapportage
date: 2019-06-12
---

Naar aanleiding van de feedback die ik gisteren heb ontvangen bij creative concepting lab, heb ik voor mezelf een testrapportage gemaakt. Hierin heb ik alle conclusies en inzichten van voorgaande onderzoeken gebundeld in één document. Dit wil ik dan ook morgen met de rest bespreken om gaten te dichten en concreet te hebben staan welke inzichten belangrijk zijn voor ons concept.

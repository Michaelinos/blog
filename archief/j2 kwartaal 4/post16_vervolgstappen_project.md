---
title: Vervolgstappen project
date: 2019-06-13
---

Ik heb met de rest van de groep gedeeld wat ik in creative concepting lab heb besproken. De belangrijkste twee dingen zijn dat wij terug moeten grijpen naar de NRC huisstijl als wij onze doelgroep willen aanspreken. Ook hadden wij een gemiste kans omdat wij niet veel met emotie hadden gedaan. Dit willen wij oplossen door een koppeling met een smartdevice zoals een ring of horloge toe te voegen. Ook hebben wij als team een nieuw motto gemaakt waar wij achter staan.

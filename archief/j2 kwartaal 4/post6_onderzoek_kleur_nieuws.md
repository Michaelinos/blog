---
title: Onderzoek kleur en nieuws
date: 2019-05-23
---

Vandaag hebben wij een onderzoek gedaan over hoe je kleur en nieuws kan combineren. Jari heeft een lat gemaakt met het kleurenspectrum en een aantal artikelen verzameld die een emotie aanspreken. Vervolgens hebben wijj een aantal testpersonen een stapel artikelen gegeven en gevraagd of zij dit willen categoriseren per kleur en wat voor emotie zij aan deze kleur zouden koppelen.

Deze tests hebben wij op de Hogeschool Rotterdam gehouden en in de binnenstad van Rotterdam. In totaal hebben wij vijf tests afgenomen. Deze zijn soepel verlopen en gedocumenteerd en wij hebben er wel een aantal interessante gegevens uit kunnen halen.

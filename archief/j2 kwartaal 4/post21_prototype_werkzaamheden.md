---
title: Ontwerpdocumentatie + prototype
date: 2019-06-20
---

In de ochtend zijn we begonnen met een lijstje te maken voor wie wat maakt in de ontwerpdocumentatie. Hierbij schrijf ik een stuk over de positionering van ons concept, het kleuren prototype en een stuk over de design crit.

Voor de rest van de dag ben ik bezig geweest met dingen verbeteren in het prototype. Ook is er nu een stuk over de koppeling van de smartwatch gemaakt.

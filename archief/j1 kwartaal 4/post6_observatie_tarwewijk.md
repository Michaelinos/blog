---
title: Observatie Tarwewijk
date: 2018-04-25
---

Vanochtend hebben we een aantal interviewvragen opgesteld voor de jongeren en zijn we rond 11.00u naar de Tarwewijk gegaan. Het plan was om zo veel mogelijk te observeren in de wijk, kijken wat mensen doen, belangrijke plekken te bezoeken en zo veel mogelijk foto’s te maken. Mochten we iemand tegen komen dan zouden we die persoon interviewen.

Het was op dit moment een vrij grauwe, koude ochtend dus we zijn geen mensen uit onze doelgroep tegen gekomen op straat. We hebben wel veel foto’s gemaakt en een indruk gekregen van de wijk. De wijk had geen fijne sfeer en het zag er erg onverzorgd uit wat wel voor een contrast zorgde in vergelijk met noord/centrum Rotterdam.

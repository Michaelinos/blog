---
title: Kickoff kwartaal 4
date: 2018-04-09
---

Vandaag was de kickoff van het laatste kwartaal van het schooljaar. Onze opdrachtgever van het EMI heeft ons een presentatie gegeven over wat er precies van ons verwacht wordt dit kwartaal en op welke wijken wij gaan focussen in Rotterdam Zuid.

Na de presentatie werden de wijken verdeeld onder de teams in onze klas. Welke wijk je kreeg werd bepaald door het trekken van lootjes. Ons team heeft de Tarwewijk gekregen.

Hierna zijn we aan de slag gegaan met research over de wijk. We hebben filmpjes gekeken, cijfers en statistieken opgezocht en gelezen over de historie van de wijk. We zijn relatief vroeg naar huis gegaan.

---
title: Feedback, workshops, interviews uitgewerkt
date: 2018-05-09
---

In de ochtend heb ik feedback gevraagd bij Mio over de planning die ik heb gemaakt en het onderzoek dat wij hebben gedaan. Hij heeft vertelt dat de planning prima was maar ik moet goed reflecteren op wat er mis is gegaan en hier rekening mee houden, de planning moet daarop aangepast worden. Ook moet je ruimte houden voor speling wanneer bijv. Iemand ziek is.

Over het onderzoek vertelde Mio dat een goede structuur noodzakelijk is. Research moeten we vergelijken met de daadwerkelijke ervaring van mensen. We moeten gerichte vragen hebben en hier een duidelijk antwoord uit halen d.m.v. fieldresearch.

Hierna hebben we gezamenlijk in de klas een inventarisatie gedaan voor onze competenties en leerdossier. Daar heb ik opgeschreven dat ik de competenties ideevorming en samenwerken bijna heb gehaald en dat ik voor de overige competenties moet onderzoeken hoe ik deze kan behalen.

Nadat ik dit formulier heb ingeleverd heb ik de interviews van gisteren uitgewerkt. We hebben audio opgenomen, hier heb ik vervolgen een transcriptie van uitgewerkt in Word.

In de middag heb ik de workshop uitstelgedrag gevolgd. Deze vond ik erg interessant en leuk gedaan. Het gaf inzichten over de psychologie achter uitstelgedrag en wat voor kenmerken mensen hebben die er erg gevoelig voor zijn. Ook heb ik nuttige tips gehad hoe ik beter met mijn uitstelgedrag om kan gaan.

---
title: Terugblik summerschool
date: 2018-06-29
---

Ik moest tijdens summerschool twee competenties herkansen. Inrichten ontwerpproces en ideevorming. Het was een intens project waar ik in een korte tijd veel deliverables heb moeten maken waarbij het uiteindelijk wel is gelukt om alles op tijd af te kunnen ronden. Voor inrichten ontwerpproces heb ik een debriefing en ontwerpproceskaart gemaakt.

Ik vind dat ik veel ben gegroeid met het inrichten in tegen stelling tot kwartaal 4. Voorheen was ik nog te veel gefocust op planning en tijd, ik heb me nu beter georiënteerd en bij het hele proces bewuste keuzes gemaakt voor welke technieken en methodes ik ga inzetten tijdens het project. Door het maken van de debrief wist ik aan welke voorwaarden en criteria mijn concept moest voldoen en wist ik wat er van mij verwacht werd. Aan de hand van nieuwe inzichten door onderzoek heb ik mijn ontwerpdoel aangepast en specifieker gemaakt.

Voor ideevorming heb ik een verslag van mijn gebruikte creatieve technieken, conceptbeschrijving en een conceptposter gemaakt. Het proces van alle creatieve technieken, inclusief het verloop, argumentatie van de keuze van technieken en foto’s heb ik uitgewerkt tot een verslag. Hierbij heb ik ook de verschillende conceptideeën en mijn uiteindelijke keuze vermeld. Deze keuze heb ik gemaakt aan de hand van mijn onderzoeksresultaten.

Nadat ik een keuze had gemaakt voor een concept door te convergeren, heb ik dit uitgewerkt in een poster met een samenvatting van het concept en hierbij de belangrijkste functies omschreven. Alle functies en hoe mijn concept het ontwerpdoel heeft opgelost, heb ik uitgewerkt in een conceptbeschrijving.

Aan de hand van mijn conceptbeschrijving/poster heb ik een prototype gemaakt. Ik vind doordat ik deze keer veel bewuster keuzes heb gemaakt, ik een concept heb kunnen ontwikkelen dat beter past bij de doelgroep dan voorheen. Door constant rekening te houden met mijn opgestelde criteria en onderzoeksresultaten heb ik gedurende het ontwerpproces een prototype kunnen ontwikkelen dat aansluit bij de wensen van de doelgroep.

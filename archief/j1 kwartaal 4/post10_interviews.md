---
title: Interviews
date: 2018-05-08
---

We hebben in de ochtend naar buurthuizen in de Tarwewijk gebeld met de vraag of we met wat interviewvragen langs mogen. Een buurthuis daarvan was gesloten en de andere zou terugbellen maar dat is niet meer gebeurt.

Rond 14.00u zijn Andor en ik naar de Tarwewijk gegaan om wat interviews af te leggen. We hebben allebei afgewisseld, de ene keer ging Andor interviewen en ik opnemen en de volgende keer andersom. In het begin was het lastig om mensen aan te spreken maar dat ging na een paar keer steeds beter. Uiteindelijk hebben we twee meisjes van 18 en één jongen van 25 geïnterviewd. Van alle drie de interviews hebben we goede inzichten en meningen over de wijk verkregen.

---
title: Onderzoeksrapport & testrapportage
date: 2018-05-31
---

Ik had allereerst het onderzoeksrapport afgemaakt. Ik had onlangs feedback gekregen op het onderzoeksrapport waarbij ik de observatie en de foto’s moest samenvoegen en mijn bronnen volgens de APA-regels moest verwerken.

Hierna heb ik het testrapport afgemaakt. Hiervan moesten alle verzamelde gegevens nog bij elkaar gevoegd worden en een conclusie uitgeschreven worden. Ook heb ik een begin gemaakt aan de design rationale, de laatste deliverable die ik moet maken.

---
title: Low-fid testplan
date: 2018-05-22
---

Met Hanno heb ik besproken wat we hebben gemaakt voor het prototype, omdat we allebei verschillende functies hebben uitgewerkt. We hebben de beste ontwerpkeuzes van ons beide gecombineerd tot één prototype. Ik heb met Hanno afgesproken dat hij vanavond thuis het prototype clickable gaat maken zodat we het morgen kunnen testen.

Samen met Andor en Hanno hebben we een opzet gemaakt voor het testplan. We hebben hypothesen opgesteld, hoe we gaan testen en wat voor vragen we gaan stellen. Ik heb afgesproken dat ik voor morgen het testplan volledig ga uitwerken.

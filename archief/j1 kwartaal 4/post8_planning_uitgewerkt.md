---
title: Planning uitgewerkt
date: 2018-05-06
---

Vandaag heb ik de complete planning uitgewerkt op Trello. We hadden al eerder een overzicht gemaakt met welke deliverables we willen gaan maken om al onze competenties aan te tonen. Aan de hand hiervan heb ik een Excel sheet gemaakt en alle deliverables onder de teamleden verdeeld zodat iedereen zijn competenties kan aantonen. Dit heb ik vervolgens per week verdeeld en in Trello gezet.

Vervolgens heb ik bij alle deliverables kleine subtaken gemaakt en de verwachte tijdsduur toegevoegd. Zodat we al gelijk een overzicht hebben wat we dit kwartaal moeten doen en in kunnen spelen op tegenvallers.

---
title: Onderzoeksrapport feedback + prototype
date: 2018-05-28
---

In de ochtend hebben we uitleg gekregen over wat er precies van ons verwacht wordt bij de expositie a.s. vrijdag. Hiervoor moet woensdag het high-fidelity prototype afgerond zijn en we moeten een poster maken van ons project.

Na de uitleg hebben we direct taken verdeeld, Hanno en ik maken het prototype af en Indra en Andor maken de poster. Ik heb me ook ingeschreven voor een afspraak om feedback te krijgen over het onderzoeksrapport dat ik heb gemaakt.

Het onderzoeksrapport was goed verzorgd alleen moeten er een paar kleine dingen aangepast worden:

-	Bronvermelding moet volgens APA-regels gedaan worden
-	Wijkobservatie moet gecombineerd worden met de gemaakte foto’s en als een conclusie uitgeschreven worden
-	Van de Interviews moet een samenvatting gemaakt worden en datum + tijd toevoegen
-	In de conclusie moet gerefereerd worden naar de bevindingen

De rest van de dag ben ik bezig geweest met het uitwerken van het prototype. 

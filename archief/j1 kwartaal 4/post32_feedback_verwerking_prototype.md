---
title: Onderzoek en verslag feedback verwerkt, low + high-fid prototype
date: 2018-06-27
---

Mijn onderzoeksrapport had wel al alle gegevens maar het was nog niet netjes uitgewerkt in hoofdstukken en met de correcte bronvermelding. Dit onderzoeksrapport heb ik daarom nu volledig uitgewerkt + bronvermelding volgens de APA-regels.

Gisteren heb ik van Mieke als feedback gekregen dat mijn verslag over de gebruikte creatieve technieken inhoudelijk alles heeft, maar er ontbrak een duidelijk scheiding tussen verschillende onderdelen en de resultaten van iedere gebruikte techniek. Deze feedback heb ik dan ook vandaag verwerkt door overal koppen toe te voegen en duidelijk aan te geven wat iedere techniek mij heeft opgeleverd.

Ook heb ik gisteren een concepttest uitgevoerd en in steekwoorden de feedback opgeschreven. Vandaag ben ik hier verder mee gegaan door de feedback van iedereen volledig uit te werken. De rest van de dag ben ik bezig geweest met het high-fidelity prototype. Ik heb aan de hand van mijn gemaakte schetsen wireframes gemaakt in Adobe XD, die ik vervolgens uit heb gewerkt tot een high-fidelity prototype.

---
title: Feedback gevraagd, testplan verbetert
date: 2018-05-23
---

In de ochtend heb ik aan Jantien feedback gevraagd op het plan van aanpak dat ik heb aangepast en het testplan dat ik gisteren heb geschreven. Op het plan van aanpak heb ik als feedback gekregen:

-	Bij bevindingen vorig kwartaal moeten we alleen beschrijven wat we daadwerkelijk meenemen naar het volgende kwartaal
-	Ontwerpvragen te algemeen
-	Onderzoeksmethoden veel specifieker beschrijven
-	Screenshots van Trello toevoegen

Het plan van aanpak hoeft niet meer aangepast worden maar ik kan dit meenemen naar volgend schooljaar.

Bij het testplan heb ik als feedback gekregen:

-	Niet testen op het HR, alleen testen in de Tarwewijk
-	Maak hypothesen die gemakkelijk testbaar zijn
-	Testmethoden beter defineren
-	Sessie veel gedetailleerd uitwerken, schrijf een script van wat er gezegd en gedaan moet worden

De rest van de dag ben ik bezig geweest met de feedback te verwerken in het nieuwe testplan. We wilden eigenlijk vandaag gaan testen maar het regende continue waardoor we hebben afgesproken de volgende dag te testen.

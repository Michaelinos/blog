---
title: Debriefing, verslag creatieve technieken en ontwerpproceskaart feedback
date: 2018-06-25
---

Ik heb allereerst een debriefing gemaakt. I.v.m. de competentie inrichten ontwerpproces is het belangrijk dat ik mijn ontwerpdoel duidelijk definieer en dit verwerk in een debriefing. In deze debriefing moet de opdrachtgever, huidige situatie, het ontwerpdoel, randvoorwaarden en tijdsplanning vermeld worden. Aan de hand van nieuwe inzichten kan het ontwerpdoel dan weer aangepast en concreter gemaakt worden.

Ook heb ik verder gewerkt aan mijn verslag over creatieve technieken en hierbij de gebruikte convergerende techniek en mijn gemaakte mindmap toegevoegd. Vorige week heb ik feedback van Robin ontvangen waarbij ik mijn gebruikte onderzoeksmethoden voor de concurrentie concreter moest maken, dit heb ik dan ook verwerkt in mijn ontwerpproceskaart.

Vervolgens ben ik feedback gaan vragen bij Jantien over mijn gemaakte documenten.

-	Bij de debriefing moet ik het ontwerpdoel n.a.v. onderzoek heel concreet maken. Een specifiekere doelgroep, behoeftes en leeftijd.
-	De opdrachtgever is nu nog Hogeschool Rotterdam. Dit moet het gekozen bedrijf TripAdvisor worden zodat het een professionele debriefing wordt.
-	Ik heb ook gezegd dat er geen huurfietssystemen zijn voor reizigers, wat niet helemaal waar is. Dit moet ik dus aanpassen.

De feedback op mijn verbeterde ontwerpproceskaart was dat deze nu in orde is, alles is helder geformuleerd en onderbouwd. Mijn verslag over creatieve technieken was nog niet helemaal af maar Jantien heeft me vertelt dat de gebruikte creatieve technieken + argumentatie voor keuzes goed was. Ook moet ik erop letten dat de bijlagen goed leesbaar zijn, ik gekantelde pagina’s toe voeg en dat alle afbeeldingen netjes recht gezet worden.

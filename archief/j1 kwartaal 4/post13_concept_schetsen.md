---
title: Concept schetsen
date: 2018-05-15
---

In de ochtend heb ik met Indra en Hanno besproken wat voor concept Andor en ik gisteren hadden verzonnen. Ze vonden het een tof idee en zijn er ook enthousiast over geworden. We zijn vervolgens begonnen met het concept op een low-fidelity manier uit te schetsen. Hierdoor kwamen we op het idee om een social feed toe te voegen waarbij gebruikers uit de Tarwewijk ervaringen en vragen met elkaar kunnen delen. Ook is er een idee ontstaan waarbij de persoonlijke voortgang wordt gevisualiseerd doormiddel van een avatar die er steeds beter uit begint te zien naarmate je opdrachten voltooid.

Nadat we de hoofdfeatures grof hadden uit geschetst, zijn Hanno en ik verder gegaan met het prototype op een low-fidelity uit te werken in Sketch/Adobe XD.

In de middag heb ik een workshop gevolgd over user testing. Hier heb ik uitleg gekregen over verschillende testmethodes en hoe je het beste hypotheses kan opstellen.

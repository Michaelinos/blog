---
title: Verslag Tarwewijk
date: 2018-04-26
---

We hebben afgesproken dat iedereen individueel zijn bevindingen van de week gaat omschrijven. Hierbij moeten een aantal vragen beantwoord worden in het verslag dat ik heb gehaald uit de CityasText presentatie. Dit verslag heb ik al grotendeels uitgewerkt vandaag, in dit verslag heb ik vertelt over de sfeer, wat de mensen doen, wat we tegen zijn gekomen, foto’s en wat voor faciliteiten er aanwezig zijn in de wijk.

Ook heb ik nog wat nuttige interviewvragen over de wijk opgesteld. In de namiddag ben ik samen met Andor teruggegaan naar de wijk om interviews af te leggen. Het was nu een stuk zonniger waardoor de wijk een veel beter indruk gaf. We zijn niet veel mensen tegen gekomen uit onze doelgroep, maar de mensen die we aan hadden gesproken hadden helaas geen interesse.

---
title: Design Rationale & Eind Expositie
date: 2018-06-01
---

In de ochtend heb ik de design rationale afgemaakt. In dit document heb ik beschreven waarom ik voor de uitgewerkte functies heb gekozen bij het concept. Dit heb ik vervolgens onderbouwd met de resultaten uit ons onderzoek. Verder heb ik ook onze keuzes met betrekking tot kleurgebruik en structuur beschreven.

Rond 13.00u was het tijd om onze stand voor de eindexpositie op te bouwen. We zijn naar het stadslab geweest om onze poster op A1 formaat uit te printen. Verder heb ik ook een mock-up van het prototype gemaakt en uitgeprint. We hebben de pitch samen met elkaar doorgenomen en rond 13.30u heb ik het prototype gepitcht aan Jantien & Mio. Dit ging erg goed, we hebben een goede beoordeling gekregen op het validatieformulier. Als feedback heb ik gekregen dat het verhaal goed was en dat we het concept goed hebben uitgelegd. Alleen moest ik ontbrekende zaken niet goed praten maar accepteren. Voor de rest was de opmaak van onze stand een beetje kaal.

De presentatie poster was nagenoeg voldoende alleen was er te veel tekst en onvoldoende hiërarchie. Er moest duidelijker aangegeven worden wat belangrijk is en wat niet. Verder ontbraken onze inzichten uit het onderzoek en kwam de stijl van de poster niet overeen met de stijl van de applicatie. Ik ben nagenoeg wel tevreden over ons resultaat van de expositie en het gehele kwartaal.

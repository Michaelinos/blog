---
title: Nieuwe teamleider + PvA werkzaamheden
date: 2018-04-18
---

Onze huidige teamleider Indra wilde graag een her electie doen dit kwartaal voor een nieuwe teamleider. We hebben allemaal gestemd en ik ben toen gekozen als nieuwe teamleider dit kwartaal. Ik heb dan ook nu haar taken daarvan overgenomen.

Vervolgens zijn we verder gegaan met het plan van aanpak. We hebben samen nieuwe ontwerpvragen verzonnen en vervolgens zijn we onderzoek gaan doen hoe we een stakeholder map moeten maken. Ik heb gegoogeld hoe deze opgebouwd moet worden en gezocht naar wat voorbeelden. We hebben vervolgens een mindmap gemaakt en zoveel mogelijk stakeholders bedacht. Dit hebben we in categorieën verdeeld en gerangschikt op prioriteit. Hanno werkt deze stakeholder map verder uit voor volgende week maandag.

Ik heb ook een begin gemaakt aan de planning en bepaalt welke deliverables we dit kwartaal moeten maken. Ik heb afgesproken het plan van aanpak voor volgende week maandag af te hebben en hier een visualisatie van te maken.

In de middag heb ik een workshop gebruikerstesten gevolgd, wat ging over het opstellen van een testplan en het uitvoeren van testen.

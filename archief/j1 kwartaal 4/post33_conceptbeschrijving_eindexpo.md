---
title: Conceptbeschrijving + Eindexpositie
date: 2018-06-28
---

Omdat ik voor synthetiseren tot concept bij de competentie ideevorming nog alleen een conceptposter had, leek het mij verstandig om een conceptbeschrijving te maken van mijn bedachte concept. Hierbij heb ik de doelgroep en huidige situatie beschreven en mijn ontwerpdoel verklaart. Aan de hand van deze gegevens en mijn gemaakte conceptposter heb ik de functies die de applicatie gaat hebben verder uitgewerkt en beschreven. Hierbij heb ik rekening gehouden met de randvoorwaarden uit mijn debriefing. Verder heb ik ook een aantal gemaakte wireframes toegevoegd ter illustratie van de functies.

Nadat de conceptbeschrijving was afgerond, heb ik bij Nathan, een klasgenoot, feedback gevraagd op mijn gemaakte document. Nathan heeft mijn document doorgelezen. Vervolgens heb ik samen met Nathan de beoordelingscriteria van het beroepsproduct conceptbeschrijving nagelopen. Hij heeft mij vertelt dat het document grotendeels in orde is en aan de eisen voldoet. Alleen moet ik behalve mijn gemaakte wireframes ook mijn conceptschetsen toevoegen aan het document. Verder moet ik beter definiëren wanneer het beschreven ontwerpdoel “ultieme ervaring” bereikt is.

Ik heb daarom het ontwerpdoel beter omschreven en een aantal gemaakte schetsen toegevoegd om mijn proces beter te laten zien. In de namiddag was de expositie van een aantal klasgenoten waarbij zij hun gemaakte prototype hebben gepresenteerd. Ik heb op de expositie rondgelopen en mensen feedback gegeven op hun gemaakte werk. De rest van de dag ben ik bezig geweest met het schrijven van mijn STARRTS.

---
title: Afwerking prototype + testrapportage
date: 2018-05-30
---

Ik had met Hanno overlegd over het stroomlijnen van de schermontwerpen die we hadden gemaakt. Vervolgens heb ik de laatste schermen afgemaakt voor de highscores, voorpagina en voortgang. Ik heb met Hanno afgesproken dat hij de rest van het prototype clickable gaat maken.

Hierna had ik begin gemaakt aan het testrapport. Voor de rest heb ik verder gewerkt aan het leerdossier.

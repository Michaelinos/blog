---
title: Plan van aanpak presenteren
date: 2018-04-23
---

Vandaag hebben we in de vroege ochtend ons plan van aanpak gepresenteerd. Dit ging niet super goed, onze fout was dat het plan van aanpak pas op het laatste moment af was, daardoor hebben we dit niet samen kunnen doornemen en hebben we dus niets voor kunnen bereid.

We hebben van Jantien en Robin wel goede feedback ontvangen waarmee we ons plan van aanpak kunnen aanpassen:

-	Plan van aanpak moet veel concreter geformuleerd zijn
-	Momenteel was de planning nog te globaal, fasen toevoegen, taken aan iedereen koppelen, strakke planning uitwerken
-	Beschrijven wat we van ons vorige kwartaal meenemen en hoe we dit gaan nu gaan gebruiken
-	Onderzoeksmethoden vaststellen en uitwerken
-	Ontwerpvragen veel concreter

Met deze feedback zijn we vervolgens gelijk aan de slag gegaan. We hebben onderzoeksmethodes vastgesteld die we willen gaan gebruiken, zoals deskresearch, observeren en interviews. Vervolgens hebben we onderzoeksvragen opgesteld waar wij graag antwoord op willen d.m.v. onze onderzoeksmethodes.

Hierna hebben we een plan opgesteld voor de interviews. We gaan a.s. woensdag door de wijk lopen en proberen zo veel mogelijk interessante foto’s te maken en te observeren. Ook hebben we met de groep afgesproken dat iedereen thuis deskresearch gaat doen naar de wijk.

In de middag ben ik naar de CityasText workshop gegaan, hier heb ik inzichten gekregen over hoe je het beste de wijk kan observeren en wat voor vragen belangrijk zijn om antwoord op te krijgen om een wijk in kaart te brengen.

---
title: Concept idee
date: 2018-05-14
---

In de ochtend heb ik gekeken naar de resultaten van ons onderzoek en hier conclusies uit getrokken. Hier kwam bijvoorbeeld uit dat de jongeren in de Tarwewijk gevoelig zijn voor meeloopgedrag, weinig regelmaat hebben in hun leven en dat er genoeg faciliteiten in de wijk beschikbaar zijn om gezond te leven maar dat hier geen gebruik van wordt gemaakt. Dit hebben we vervolgens gezamenlijk als team besproken.

Afgelopen woensdag is Hanno naar een workshop geweest over creatieve technieken. Hier heeft hij geleerd over de creatieve techniek toevalstreffers. Dit is een techniek waarbij je een willekeurig boek pakt en naar een willekeurig hoofdstuk en paragraaf gaat en vervolgens het eerste zelfstandig naamwoord wordt pakt.

Vervolgens schrijf je dit woord op papier, zet je een timer van twee minuten en schrijf je gezamenlijk als groep allerlei woorden op die met het gekozen woord te maken heeft. Dit doe je vervolgens met een aantal woorden. Als je hier klaar mee bent arceer je woorden die potentieel nut hebben voor je concept en werk je dit verder uit hoe je dit kan terugkoppelen. Dit hebben we gedaan met een stuk of vijf woorden die wij verder uit hebben gewerkt om in de toekomst op terug te blikken.

In de namiddag heb ik met Andor verschillende andere creatieve technieken toegepast omdat we nog geen concept hadden verzonnen. We zijn allereerst begonnen met een mindmap te maken van de Tarwewijk en alles hierover te noteren. Vervolgens hebben we gereflecteerd op de onderzoeksresultaten en ons vorige concept.

Bij het vorige concept hadden we een soort app waarbij het hele bravo model verwerkt was maar de focus lag op ritme. Door kleine taken te doen kon je ritme opbouwen in je leven. Die feedback die we hierop hadden gekregen was dat we beter het hele bravo model hierin kunnen verwerken.

Dit hebben we gecombineerd met het feit dat de beschikbare faciliteiten in de wijk niet gebruikt worden door de jeugd. Hierdoor kwamen we op het idee om een soort kaart van de Tarwewijk te maken waar je allerlei faciliteiten kunt terugvinden die te maken met gezondheid. Ook worden hier opdrachten weergegeven die je kan doen om gezonder te worden. Naarmate je meer opdrachten voltooid heb je persoonlijke progressie en wijkprogressie die gevisualiseerd wordt in de app.

---
title: Begin aan nieuw PvA
date: 2018-04-11
---

Vandaag hebben we een begin gemaakt aan het nieuwe plan van aanpak. Aan plan van aanpak gewerkt. We hebben geïnventariseerd wat er aangepast moet worden en bepaalt wat we er nog meer in gaan verwerken.

De debrief moet een beetje aangepast worden, er moet een stakeholder map komen, onderzoeksmethoden vaststellen en een nieuwe planning maken. Hanno past de debrief aan, ik maak de planning en de rest gaan we gezamenlijk als team doen.
